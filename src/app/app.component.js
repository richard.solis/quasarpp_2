var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { loginPage } from '../pages/login/login';
import { ConfirmacionE } from '../pages/confirmacion-e/confirmacion-e';
import { MantenimientoPage } from '../pages/mantenimiento/mantenimiento';
import { Oportunidad } from '../pages/oportunidad/oportunidad';
import { TasksPage } from '../pages/tasks/tasks';
import { Geolocation } from '@ionic-native/geolocation';
import { SessionService } from "../services/session.service";
import { InterInstallIn } from '../pages/inter-install-in/inter-install-in';
import { UserProfile } from '../broadcasting/util/user-profile';
import { ContactsPage } from '../pages/contacts/contacts';
import { MeasurementsMPage } from '../pages/measurements-m/measurements-m';
var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, geolocation, sesionS, menuCtrl, profile, menu) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.geolocation = geolocation;
        this.sesionS = sesionS;
        this.menuCtrl = menuCtrl;
        this.profile = profile;
        this.menu = menu;
        this.rootPage = loginPage;
        this.task = "";
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Tiendas', component: HomePage },
            { title: 'Tarea', component: TasksPage },
            { title: 'Instalación', component: InterInstallIn },
            { title: 'Inventario', component: InterInstallIn },
            { title: 'Mantenimiento', component: MantenimientoPage },
            { title: 'Confirmación de Espacio', component: ConfirmacionE },
            { title: 'Tomar Medidas', component: MeasurementsMPage },
            //{ title: 'Incidencia', component: Incidencia },
            { title: 'Contacto', component: ContactsPage },
            { title: 'Oportunidad Comercial', component: Oportunidad },
        ];
    }
    MyApp.prototype.close = function () {
        this.menuCtrl.close();
        this.sesionS.destroy('token');
        this.sesionS.destroy('user');
        this.nav.setRoot(loginPage);
    };
    MyApp.prototype.ionViewDidLoad = function () {
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            if (_this.sesionS.getObject('user')) {
                _this.user = _this.sesionS.getObject('user').firstname;
            }
            if (_this.sesionS.getObject('tienda')) {
                _this.store = _this.sesionS.getObject('tienda').store.name;
            }
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
        this.menu.swipeEnable(true);
        this.registerStringBroadcast();
    };
    MyApp.prototype.openPage = function (page) {
        console.log(page);
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.title == 'Instalación') {
            this.task = 'Instalación';
        }
        else if (page.title == 'Inventario') {
            this.task = 'Inventario';
        }
        else if (page.title == 'Mantenimiento') {
            this.task = 'Desinstalación';
        }
        else if (page.title == 'Confirmación de Espacio') {
            this.task = 'Confirmación';
        }
        else if (page.title == 'Tomar Medidas') {
            this.task = 'Medidas';
        }
        else if (page.title == 'Contacto') {
            this.task = 'Contactos';
        }
        this.nav.setRoot(page.component, {
            task: this.task,
        });
    };
    MyApp.prototype.registerStringBroadcast = function () {
        var _this = this;
        this.profile.on()
            .subscribe(function (value) {
            console.log(value);
            if (value.name) {
                _this.user = value.name;
            }
            if (value.tienda) {
                _this.store = value.tienda;
            }
        });
    };
    return MyApp;
}());
__decorate([
    ViewChild(Nav),
    __metadata("design:type", Nav)
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Component({
        templateUrl: 'app.html',
        providers: [Geolocation, UserProfile]
    }),
    __metadata("design:paramtypes", [Platform,
        StatusBar,
        SplashScreen,
        Geolocation,
        SessionService,
        MenuController,
        UserProfile,
        MenuController])
], MyApp);
export { MyApp };
//# sourceMappingURL=app.component.js.map