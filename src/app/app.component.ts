import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { Ubicacion } from '../pages/ubicacion/ubicacion';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { loginPage } from '../pages/login/login';
import { ConfirmacionE } from '../pages/confirmacion-e/confirmacion-e';
import { Contacto } from '../pages/contacto/contacto';
import { Incidencia } from '../pages/incidencia/incidencia';
import { Instalacion } from '../pages/instalacion/instalacion';
import { TareasPenPage } from '../pages/tareas-pen/tareas-pen';
import { TareasUninstallPage } from '../pages/tareas-uninstall/tareas-uninstall';
import { TareasInventoryPage } from '../pages/tareas-inventory/tareas-inventory';
import { TareasConfirmPage } from '../pages/tareas-confirm/tareas-confirm';
import { TareasMeasurementsPage } from '../pages/tareas-measurements/tareas-measurements';
import { MantenimientoPage } from '../pages/mantenimiento/mantenimiento';
import { Medidas } from '../pages/medidas/medidas';
import { Oportunidad } from '../pages/oportunidad/oportunidad';
import { Inventario } from '../pages/inventario/inventario'; 
import { Tarea } from '../pages/tarea/tarea'; 
import { TasksPage } from '../pages/tasks/tasks'; 
import { Geolocation } from '@ionic-native/geolocation';
import { InterInventario } from '../pages/inter-inventario/inter-inventario';
import { SessionService } from "../services/session.service";
import { InterInstallIn } from '../pages/inter-install-in/inter-install-in';
import { UserProfile } from '../broadcasting/util/user-profile';
import { ContactsPage } from '../pages/contacts/contacts';
import { MeasurementsMPage } from '../pages/measurements-m/measurements-m';




@Component({
  templateUrl: 'app.html',
  providers:[Geolocation, UserProfile]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = loginPage;
  public user: any;
  location: any ;
  public store: any;
  public ubicationM: String;

  pages: Array<{title: string, component: any}>;
  task = "";
  constructor(public platform: Platform, 
              public statusBar: StatusBar,
              public splashScreen: SplashScreen, 
              public geolocation:Geolocation,
              public sesionS:SessionService,
              public menuCtrl: MenuController,
              private profile: UserProfile,
              public menu: MenuController,) {
    this.initializeApp(); 
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Tiendas', component: HomePage },
      { title: 'Tarea', component: TasksPage },
      { title: 'Instalación', component: InterInstallIn },
      { title: 'Inventario', component: InterInstallIn },
      { title: 'Mantenimiento', component: MantenimientoPage },
      { title: 'Confirmación de Espacio', component: ConfirmacionE },
      { title: 'Tomar Medidas', component: MeasurementsMPage },
      //{ title: 'Incidencia', component: Incidencia },
      { title: 'Contacto', component: ContactsPage },
      { title: 'Oportunidad Comercial', component: Oportunidad },
    ];
  }

  close(){
    this.menuCtrl.close();
    this.sesionS.destroy('token');
    this.sesionS.destroy('user');
    this.nav.setRoot(loginPage);
  }

  ionViewDidLoad(){
    
  }

  initializeApp() {    
    this.platform.ready().then(() => {
      if(this.sesionS.getObject('user')){
        this.user = this.sesionS.getObject('user').firstname;
      }
      if(this.sesionS.getObject('tienda')){

        this.store = this.sesionS.getObject('tienda').store.name;
      } 
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.menu.swipeEnable(true);
    this.registerStringBroadcast();
  }

  openPage(page) {
    console.log(page);
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.title == 'Instalación'){
      this.task = 'Instalación';
    }
    else if(page.title == 'Inventario'){
      this.task = 'Inventario';
    }
    else if(page.title == 'Mantenimiento'){
      this.task = 'Desinstalación';
    }
    else if(page.title == 'Confirmación de Espacio'){
      this.task = 'Confirmación';
    }
    else if(page.title == 'Tomar Medidas'){
      this.task = 'Medidas';
    }
    else if(page.title == 'Contacto'){
      this.task = 'Contactos';
    }
    this.nav.setRoot(page.component, {
      task:this.task,
    });
  }

  registerStringBroadcast() {
    this.profile.on()
      .subscribe(value => {
        console.log(value);
        if(value.name){
          this.user = value.name;
        }
        if(value.tienda){
          this.store = value.tienda;
        }
      });
    
  }
}
