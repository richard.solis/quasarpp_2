var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CookieModule } from 'ngx-cookie';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Tarea } from '../pages/tarea/tarea';
import { loginPage } from '../pages/login/login';
import { Ubicacion } from '../pages/ubicacion/ubicacion';
import { ConfirmacionE } from '../pages/confirmacion-e/confirmacion-e';
import { Contacto } from '../pages/contacto/contacto';
import { Incidencia } from '../pages/incidencia/incidencia';
import { Instalacion } from '../pages/instalacion/instalacion';
import { MantenimientoPage } from '../pages/mantenimiento/mantenimiento';
import { Medidas } from '../pages/medidas/medidas';
import { InterInventario } from '../pages/inter-inventario/inter-inventario';
import { InterInstall } from '../pages/inter-install/inter-install';
import { InterInstallIn } from '../pages/inter-install-in/inter-install-in';
import { Oportunidad } from '../pages/oportunidad/oportunidad';
import { Inventario } from '../pages/inventario/inventario';
import { TasksPage } from '../pages/tasks/tasks';
import { AuthModule } from "../auth/auth.module";
import { HttpModule } from "@angular/http";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleMaps } from '@ionic-native/google-maps';
import { SessionService } from '../services/session.service';
import { StoreService } from '../services/store.service';
import { TasksStoreService } from '../services/tasksstore.service';
import { CustomAuthHttp } from '../auth/custom-auth-http';
import { KeysPipe } from '../pages/pipe/pipeObject';
import { TareasPenPage } from '../pages/tareas-pen/tareas-pen';
import { TasksInstallService } from '../services/tasksinstall.service';
import { TasksUnistallService } from '../services/tasksunistall.service';
import { TasksMeasuService } from '../services/tasksmeasurements.service';
import { TasksInventoryService } from '../services/tasksinventary.service';
import { TasksConfirmService } from '../services/tasksconfirm.service';
import { TareasUninstallPage } from '../pages/tareas-uninstall/tareas-uninstall';
import { TareasInventoryPage } from '../pages/tareas-inventory/tareas-inventory';
import { TareasConfirmPage } from '../pages/tareas-confirm/tareas-confirm';
import { TareasMeasurementsPage } from '../pages/tareas-measurements/tareas-measurements';
import { TasksIssue } from '../services/tasksissue.service';
import { CMotiveService } from '../services/campaignsmotive.service';
import { CSubMotiveService } from '../services/campaignssubmotive.service';
import { CTargetService } from '../services/campaignsissuetarget.service';
import { CStatus } from '../services/campaignsissuestatus.service';
import { CCAtegoriaService } from '../services/campaignsissuecategoria.service';
import { CInstallStatusService } from '../services/campaignsinstallstatus.service';
import { CImplementers } from '../services/campaignsimplementers.service';
import { TContact } from "../services/taskscontact.service";
import { ContactsPage } from '../pages/contacts/contacts';
import { ContactsInterPage } from '../pages/contacts-inter/contacts-inter';
import { StoreContact } from '../services/storecontact.service';
import { PTasksConfirm } from '../services/tasksconfirmpost.service';
import { PTasksInventory } from '../services/tasksinventorypost.service';
import { ConfirmInterPage } from '../pages/confirm-inter/confirm-inter';
import { MeasurementsInterPage } from '../pages/measurements-inter/measurements-inter';
import { PTasksMeasurement } from '../services/tasksmeasurementpost.service';
import { Broadcaster } from '../broadcasting/util/broacaster';
import { InterUninstallPage } from '../pages/inter-uninstall/inter-uninstall';
import { PTasksUnistall } from '../services/tasksunistallpost.service';
import { OpportunitiesS } from '../services/opportunitiespost.service';
import { Oportunidades } from '../services/opportunities.service';
import { MaintenanceS } from '../services/maintenance.service';
import { CampaignsCode } from '../services/bycode.service';
import { MeasurementsMPage } from '../pages/measurements-m/measurements-m';
import { MapsService } from '../services/maps.service';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        declarations: [
            MyApp,
            HomePage,
            loginPage,
            ConfirmacionE,
            Contacto,
            Incidencia,
            Instalacion,
            MantenimientoPage,
            Medidas,
            Oportunidad,
            Inventario,
            InterInventario,
            InterInstall,
            InterInstallIn,
            Tarea,
            Ubicacion,
            TasksPage,
            KeysPipe,
            TareasPenPage,
            TareasUninstallPage,
            TareasInventoryPage,
            TareasConfirmPage,
            TareasMeasurementsPage,
            ContactsPage,
            ContactsInterPage,
            ConfirmInterPage,
            MeasurementsInterPage,
            InterUninstallPage,
            MeasurementsMPage,
        ],
        imports: [
            BrowserModule,
            IonicModule.forRoot(MyApp),
            HttpModule,
            AuthModule,
            CookieModule.forRoot()
        ],
        bootstrap: [IonicApp],
        entryComponents: [
            MyApp,
            HomePage,
            loginPage,
            ConfirmacionE,
            Contacto,
            Incidencia,
            Instalacion,
            MantenimientoPage,
            Medidas,
            Oportunidad,
            Inventario,
            InterInventario,
            InterInstall,
            InterInstallIn,
            Tarea,
            Ubicacion,
            TasksPage,
            TareasPenPage,
            TareasUninstallPage,
            TareasInventoryPage,
            TareasConfirmPage,
            TareasMeasurementsPage,
            ContactsPage,
            ContactsInterPage,
            ConfirmInterPage,
            MeasurementsInterPage,
            InterUninstallPage,
            MeasurementsMPage,
        ],
        providers: [
            StatusBar,
            Broadcaster,
            SplashScreen,
            GoogleMaps,
            SessionService,
            StoreService,
            TasksStoreService,
            TasksInstallService,
            TasksUnistallService,
            TasksInventoryService,
            TasksConfirmService,
            CustomAuthHttp,
            TasksMeasuService,
            TasksIssue,
            CMotiveService,
            CSubMotiveService,
            CTargetService,
            CStatus,
            CCAtegoriaService,
            CInstallStatusService,
            CImplementers,
            TContact,
            StoreContact,
            PTasksConfirm,
            PTasksInventory,
            PTasksMeasurement,
            PTasksUnistall,
            OpportunitiesS,
            Oportunidades,
            MaintenanceS,
            CampaignsCode,
            MapsService,
            { provide: ErrorHandler, useClass: IonicErrorHandler }
        ]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map