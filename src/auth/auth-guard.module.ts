/*
import { Injectable } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CanActivate } from '@angular/router';
import { UserService } from "../services/user.service";
import { AppSettings } from '../app.settings';
import { Location } from '@angular/common';
import { loginPage } from '../pages/login/login';
import { touchRote } from '../services/touchrote.service';

 
@Injectable()
export class AuthGuard implements CanActivate {

  public route;

  constructor(
    private navCtrl: NavController, 
    private userServivice: UserService,
    private plat: Location,
    private touch:touchRote
    ) { }

  canActivate() {

    if (this.userServivice.isAuthenticated()) {
      // logged in so return true
      this.touch.postTouch({path: this.plat.path()})
       .subscribe( response => {
       });
     return true;
    }

    // not logged in so redirect to login page
    localStorage.clear();
    this.navCtrl.setRoot(loginPage);
    return false;
  }
} */

