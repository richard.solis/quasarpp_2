var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { AuthHttp as JwtAuthHttp } from 'angular2-jwt';
//import { loginPage } from '../pages/login';
var CustomAuthHttp = (function () {
    function CustomAuthHttp(authHttp) {
        this.authHttp = authHttp;
    }
    CustomAuthHttp.prototype.isUnauthorized = function (status) {
        return status === 0 || status === 401;
    };
    CustomAuthHttp.prototype.authIntercept = function (response) {
        var sharableResponse = response.share();
        sharableResponse.subscribe(null, function (err) {
            // Other error handling may be added here, such as refresh token …
        });
        return sharableResponse;
    };
    CustomAuthHttp.prototype.setGlobalHeaders = function (headers, request) {
        this.authHttp.setGlobalHeaders(headers, request);
    };
    CustomAuthHttp.prototype.request = function (url, options) {
        return this.authIntercept(this.authHttp.request(url, options));
    };
    CustomAuthHttp.prototype.get = function (url, options) {
        return this.authIntercept(this.authHttp.get(url, options));
    };
    CustomAuthHttp.prototype.post = function (url, body, options) {
        return this.authIntercept(this.authHttp.post(url, body, options));
    };
    CustomAuthHttp.prototype.put = function (url, body, options) {
        return this.authIntercept(this.authHttp.put(url, body, options));
    };
    CustomAuthHttp.prototype.delete = function (url, options) {
        return this.authIntercept(this.authHttp.delete(url, options));
    };
    CustomAuthHttp.prototype.patch = function (url, body, options) {
        return this.authIntercept(this.authHttp.patch(url, options));
    };
    CustomAuthHttp.prototype.head = function (url, options) {
        return this.authIntercept(this.authHttp.head(url, options));
    };
    CustomAuthHttp.prototype.options = function (url, options) {
        return this.authIntercept(this.authHttp.options(url, options));
    };
    return CustomAuthHttp;
}());
CustomAuthHttp = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [JwtAuthHttp])
], CustomAuthHttp);
export { CustomAuthHttp };
//# sourceMappingURL=custom-auth-http.js.map