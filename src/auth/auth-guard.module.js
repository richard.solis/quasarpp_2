var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserService } from "../services/user.service";
import { Location } from '@angular/common';
import { loginPage } from '../pages/login/login';
import { touchRote } from '../services/touchrote.service';
var AuthGuard = (function () {
    function AuthGuard(navCtrl, userServivice, plat, touch) {
        this.navCtrl = navCtrl;
        this.userServivice = userServivice;
        this.plat = plat;
        this.touch = touch;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.userServivice.isAuthenticated()) {
            // logged in so return true
            this.touch.postTouch({ path: this.plat.path() })
                .subscribe(function (response) {
            });
            return true;
        }
        // not logged in so redirect to login page
        localStorage.clear();
        this.navCtrl.setRoot(loginPage);
        return false;
    };
    return AuthGuard;
}());
AuthGuard = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [NavController,
        UserService,
        Location,
        touchRote])
], AuthGuard);
export { AuthGuard };
//# sourceMappingURL=auth-guard.module.js.map