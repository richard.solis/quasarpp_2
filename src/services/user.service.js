var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthHttp } from "angular2-jwt";
var UserService = (function () {
    function UserService(http, httpNoAuth) {
        this.http = http;
        this.httpNoAuth = httpNoAuth;
    }
    UserService.prototype.login = function (params) {
        var body = JSON.stringify(params);
        var headers = new Headers({
            'Content-Type': 'application/json',
        });
        var options = new RequestOptions({ headers: headers });
        return this.httpNoAuth.post(AppSettings.BASE_PATH + AppSettings.LOGIN, body, options)
            .map(function (res) {
            var response = res.json();
            return response;
        })
            .catch(this.handleError);
    };
    UserService.prototype.handleError = function (error) {
        console.error(error);
        return Observable.throw(error.json.error || 'Server error');
    };
    UserService.prototype.isAuthenticated = function () {
        if (sessionStorage.getItem('token')) {
            return true;
        }
        return false;
    };
    return UserService;
}());
UserService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [AuthHttp, Http])
], UserService);
export { UserService };
//# sourceMappingURL=user.service.js.map