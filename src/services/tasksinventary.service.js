var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";
import { CustomAuthHttp } from '../auth/custom-auth-http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
var TasksInventoryService = (function () {
    function TasksInventoryService(http, httpNoAuth) {
        this.http = http;
        this.httpNoAuth = httpNoAuth;
    }
    TasksInventoryService.prototype.getTasksInventary = function (id) {
        return this.http.get(AppSettings.BASE_PATH + AppSettings.TASKSINVENTORY + id)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.getMaterialsInventary = function () {
        return this.http.get(AppSettings.BASE_PATH + AppSettings.MATERIALSINVENTORY)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.getClienteInventary = function () {
        return this.http.get(AppSettings.BASE_PATH + AppSettings.CLIENTSINVENTORY)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.getElementInventary = function () {
        return this.http.get(AppSettings.BASE_PATH + AppSettings.ELEMENTSINVENTORY)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.getCategoriInventary = function () {
        return this.http.get(AppSettings.BASE_PATH + AppSettings.CATEGORYINVENTORY)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.setInventory = function (params) {
        return this.http.post(AppSettings.BASE_PATH + AppSettings.TASKSINVENTORY, params)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.setMaterialsInventory = function (params) {
        return this.http.post(AppSettings.BASE_PATH + AppSettings.MATERIALSINVENTORY, params)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.setClienteInventory = function (params) {
        return this.http.post(AppSettings.BASE_PATH + AppSettings.CLIENTSINVENTORY, params)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.setElementsInventory = function (params) {
        return this.http.post(AppSettings.BASE_PATH + AppSettings.ELEMENTSINVENTORY, params)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.setCategoriInventory = function (params) {
        return this.http.post(AppSettings.BASE_PATH + AppSettings.CATEGORYINVENTORY, params)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksInventoryService.prototype.handleError = function (error) {
        console.error(error);
        return Observable.throw(error.json.error || 'Server error');
    };
    TasksInventoryService.prototype.isAuthenticated = function () {
        if (sessionStorage.getItem('token')) {
            return true;
        }
        return false;
    };
    return TasksInventoryService;
}());
TasksInventoryService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [CustomAuthHttp, Http])
], TasksInventoryService);
export { TasksInventoryService };
//# sourceMappingURL=tasksinventary.service.js.map