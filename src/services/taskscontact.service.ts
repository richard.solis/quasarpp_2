import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";
import { CustomAuthHttp } from '../auth/custom-auth-http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TContact {
    constructor(private http:CustomAuthHttp, private httpNoAuth:Http) { }

   getTContact(id: String){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.TASKSCONTACTS + id)
      .map((res:Response) => {
        let response = res.json();
        return response;
 
      })
      .catch(this.handleError);
  }

  getTContactNumber(id: String){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CONTACT_NUMBER + id)
      .map((res:Response) => {
        let response = res.json();
        return response;
      })
      .catch(this.handleError);
  }

  getTContactPrueba(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CONTACTPRUEBA )
      .map((res:Response) => {
        let response = res.json();
        return response;
 
      })
      .catch(this.handleError); 
  }

  PutContact(id: String, params){
    return this.http.put(AppSettings.BASE_PATH + AppSettings.STORECONTACTS + id, params)
      .map((res:Response) => {
        let response = res.json();
        return response;
 
      })
      .catch(this.handleError);
  }



  handleError(error) {
    return Observable.throw(error.json.error || 'Server error');
  }

  isAuthenticated(){
    if(sessionStorage.getItem('token')){
      return true;
    }
    return false;
  }

}