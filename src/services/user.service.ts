import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthHttp } from "angular2-jwt";

@Injectable()
export class UserService {
    constructor(private http:AuthHttp, private httpNoAuth:Http) { }

    login(params:Object){
    let body = JSON.stringify(params);
    let headers = new Headers({
      'Content-Type': 'application/json',
    });
    let options = new RequestOptions({ headers: headers });

    return this.httpNoAuth.post(AppSettings.BASE_PATH + AppSettings.LOGIN, body, options)
      .map((res:Response) => {
        let response = res.json();
        return response;

      })
      .catch(this.handleError);
  }

  handleError(error) {
    console.error(error);
    return Observable.throw(error.json.error || 'Server error');
  }

  isAuthenticated(){
    if(sessionStorage.getItem('token')){
      return true;
    }
    return false;
  }

}