import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";
import { CustomAuthHttp } from '../auth/custom-auth-http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class CInstallStatusService {
    constructor(private http:CustomAuthHttp, private httpNoAuth:Http) { }

   getCIStatus(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CAMPAIGNSINSTALLSTATUS)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  handleError(error) {
    console.error(error);
    return Observable.throw(error.json.error || 'Server error');
  }

  isAuthenticated(){
    if(sessionStorage.getItem('token')){
      return true;
    }
    return false;
  }

}