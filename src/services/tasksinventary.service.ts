import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";
import { CustomAuthHttp } from '../auth/custom-auth-http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TasksInventoryService {
    constructor(private http:CustomAuthHttp, private httpNoAuth:Http) { }

   getTasksInventary(id){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.TASKSINVENTORY + id)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
    }

    getMaterialsInventary(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.MATERIALSINVENTORY )
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
    }

    getClienteInventary(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CLIENTSINVENTORY )
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
    }

    getElementInventary(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.ELEMENTSINVENTORY )
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
    }

    getCategoriInventary(){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.CATEGORYINVENTORY )
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
    }




  setInventory(params){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASKSINVENTORY, params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  setMaterialsInventory(params){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.MATERIALSINVENTORY, params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  setClienteInventory(params){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.CLIENTSINVENTORY, params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  setElementsInventory(params){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.ELEMENTSINVENTORY, params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  setCategoriInventory(params){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.CATEGORYINVENTORY, params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  handleError(error) {
    console.error(error);
    return Observable.throw(error.json.error || 'Server error');
  }

  isAuthenticated(){
    if(sessionStorage.getItem('token')){
      return true;
    }
    return false;
  }

}