import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";
import { CustomAuthHttp } from '../auth/custom-auth-http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TasksMeasuService {
    constructor(private http:CustomAuthHttp, private httpNoAuth:Http) { }

   getTasksMeasurements(id){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.TASKSMEASUREMENTS + id)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  setMeasurements(params){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.TASKSMEASUREMENTS, params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  handleError(error) {
    console.error(error);
    return Observable.throw(error.json.error || 'Server error');
  }

  isAuthenticated(){
    if(sessionStorage.getItem('token')){
      return true;
    }
    return false;
  }

}