var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";
import { CustomAuthHttp } from '../auth/custom-auth-http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
var TasksUnistallService = (function () {
    function TasksUnistallService(http, httpNoAuth) {
        this.http = http;
        this.httpNoAuth = httpNoAuth;
    }
    TasksUnistallService.prototype.getTasksUnistall = function (id) {
        return this.http.get(AppSettings.BASE_PATH + AppSettings.TASKSUNISTALL + id)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksUnistallService.prototype.setUninstall = function (params) {
        return this.http.post(AppSettings.BASE_PATH + AppSettings.TASKSUNISTALL, params)
            .map(function (res) {
            var response = res.json();
            console.log(response);
            return response;
        })
            .catch(this.handleError);
    };
    TasksUnistallService.prototype.handleError = function (error) {
        console.error(error);
        return Observable.throw(error.json.error || 'Server error');
    };
    TasksUnistallService.prototype.isAuthenticated = function () {
        if (sessionStorage.getItem('token')) {
            return true;
        }
        return false;
    };
    return TasksUnistallService;
}());
TasksUnistallService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [CustomAuthHttp, Http])
], TasksUnistallService);
export { TasksUnistallService };
//# sourceMappingURL=tasksunistall.service.js.map