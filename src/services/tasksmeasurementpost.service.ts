import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app.settings';
import { Observable } from "rxjs";
import { CustomAuthHttp } from '../auth/custom-auth-http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PTasksMeasurement{
  
    constructor(private http:CustomAuthHttp, private httpNoAuth:Http) { }


   postTMeasurement(params:object){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.PTASKSMEASUREMENT , params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  setTMeasurement(params){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.PTASKSMEASUREMENT, params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  getMedidasCode(id,id_store){
    return this.http.get(AppSettings.BASE_PATH + AppSettings.TASKSSTORE + AppSettings.DETAILMEASUREMENTS + id + "/" + id_store)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  setMedidasCode(params){
    return this.http.post(AppSettings.BASE_PATH + AppSettings.DETAILMEASUREMENTS, params)
      .map((res:Response) => {
        let response = res.json();
        console.log(response);
        return response;
 
      })
      .catch(this.handleError);
  }

  handleError(error) {
    console.error(error);
    return Observable.throw(error.json.error || 'Server error');
  }

  isAuthenticated(){
    if(sessionStorage.getItem('token')){
      return true;
    }
    return false;
  }

}