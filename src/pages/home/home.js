var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Nav, LoadingController, Platform, MenuController } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { StoreService } from "../../services/store.service";
import { TasksPage } from '../tasks/tasks';
import { CImplementers } from '../../services/campaignsimplementers.service';
import { TContact } from "../../services/taskscontact.service";
import { UserProfile } from '../../broadcasting/util/user-profile';
import { MapsService } from '../../services/maps.service';
import { Contacto } from '../contacto/contacto';
import { Geolocation } from '@ionic-native/geolocation';
import { loginPage } from '../login/login';
var HomePage = (function () {
    function HomePage(navCtrl, navParams, storeS, plt, sessionS, modalCtrl, camImplementers, contact, loadingCtrl, profile, geolocation, maps, menu) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storeS = storeS;
        this.plt = plt;
        this.sessionS = sessionS;
        this.modalCtrl = modalCtrl;
        this.camImplementers = camImplementers;
        this.contact = contact;
        this.loadingCtrl = loadingCtrl;
        this.profile = profile;
        this.geolocation = geolocation;
        this.maps = maps;
        this.menu = menu;
        this.tiendaGeneral = [
            { texto: "ATE" },
            { texto: "Olivos" },
            { texto: "San Martin de Porres" },
            { texto: "San Borja" },
            { texto: "La Victoria" }
        ];
        this.tiendass = [];
        this.nearstores = [];
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.plt.registerBackButtonAction(function () {
            _this.plt.exitApp();
        });
        this.menu.swipeEnable(false);
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.storeS.getRoute(this.sessionS.getObject('user')._id)
            .subscribe(function (response) {
            _this.tiendass = response.result;
            console.log(_this.tiendass);
            for (var tiendi in _this.tiendass) {
                console.log(_this.tiendass[tiendi].store);
            }
            _this.plt.ready().then(function (readySource) {
                _this.geolocation.getCurrentPosition().then(function (pos) {
                    console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
                    _this.maps.getMaps(pos.coords.longitude, pos.coords.latitude)
                        .subscribe(function (response) {
                        _this.nearstores = response.result;
                        console.log(_this.nearstores);
                        loader.dismiss();
                    });
                });
            });
            loader.dismiss();
        });
    };
    HomePage.prototype.close = function () {
        this.sessionS.destroy('token');
        this.sessionS.destroy('user');
        this.navCtrl.setRoot(loginPage);
    };
    HomePage.prototype.searchStore = function () {
        this.navCtrl.push(Contacto);
    };
    HomePage.prototype.tienda = function (_tienda) {
        if (!_tienda.store) {
            _tienda.store = JSON.parse(JSON.stringify(_tienda));
        }
        this.sessionS.setObject('tienda', _tienda);
        this.navCtrl.setRoot(TasksPage);
        this.profile.fire({ tienda: _tienda.store.name });
        //this.navCtrl.push(TasksPage);
    };
    return HomePage;
}());
__decorate([
    ViewChild(Nav),
    __metadata("design:type", Nav)
], HomePage.prototype, "nav", void 0);
HomePage = __decorate([
    Component({
        selector: 'page-home',
        templateUrl: 'home.html',
        providers: [Geolocation]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        StoreService,
        Platform,
        SessionService,
        ModalController,
        CImplementers,
        TContact,
        LoadingController,
        UserProfile,
        Geolocation,
        MapsService,
        MenuController])
], HomePage);
export { HomePage };
//# sourceMappingURL=home.js.map