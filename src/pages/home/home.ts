import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Modal, Nav, LoadingController, Platform, MenuController } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { StoreService } from "../../services/store.service";
import { TasksPage } from '../tasks/tasks';
import { Incidencia } from '../incidencia/incidencia';
import { CImplementers } from '../../services/campaignsimplementers.service';
import { TasksStoreService } from "../../services/tasksstore.service";
import { TContact } from "../../services/taskscontact.service";
import { UserProfile } from '../../broadcasting/util/user-profile';
import { MapsService } from '../../services/maps.service';
import { Contacto } from '../contacto/contacto';
import { Geolocation } from '@ionic-native/geolocation';
import { loginPage } from '../login/login';
import { touchRote } from '../../services/touchrote.service';

@Component({ 
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[Geolocation]
})
export class HomePage {
  @ViewChild(Nav) nav: Nav;

  tiendaGeneral:Array<tiendas> = [
    { texto:"ATE" },
    { texto:"Olivos" },
    { texto:"San Martin de Porres" },
    { texto:"San Borja" },
    { texto:"La Victoria" }
    ];

    tiendass = [];
    nearstores = [];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public storeS:StoreService,
    public plt: Platform, 
    public sessionS:SessionService, 
    public modalCtrl: ModalController,
    public camImplementers:CImplementers,
    public contact:TContact,
    public loadingCtrl: LoadingController,
    private profile:UserProfile,
    public geolocation:Geolocation,
    public  maps:MapsService,
    public menu:MenuController,
    public touch: touchRote  ) {  }

  ionViewDidLoad() {
    this.plt.registerBackButtonAction(() => {
        this.plt.exitApp();
      });
   this.menu.swipeEnable(false);
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.storeS.getRoute(this.sessionS.getObject('user')._id)
    .subscribe(response => {
      this.tiendass=response.result;
      console.log(this.tiendass);
      for(var tiendi in this.tiendass){
        console.log(this.tiendass[tiendi].store);
      }
      this.plt.ready().then((readySource) => {
        this.geolocation.getCurrentPosition().then(pos => {
          console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
          this.maps.getMaps(pos.coords.longitude, pos.coords.latitude)
          .subscribe(response => {
            this.nearstores = response.result;
            console.log(this.nearstores);
            loader.dismiss();
            this.touch.postTouch({path: 'Home'})
             .subscribe( response => {
             });
          });
        });
      });
      loader.dismiss();
    });
  }

  close(){
    this.sessionS.destroy('token');
    this.sessionS.destroy('user');
    this.navCtrl.setRoot(loginPage);
  }
  
  searchStore(){
      this.navCtrl.push(Contacto);
  }

  tienda(_tienda){
    if(!_tienda.store){
      _tienda.store = JSON.parse(JSON.stringify(_tienda));
    }
    this.sessionS.setObject('tienda', _tienda);
    this.navCtrl.setRoot(TasksPage);
   
    this.profile.fire({tienda: _tienda.store.name});
    //this.navCtrl.push(TasksPage);
    this.touch.postTouch({path: 'Home/' + _tienda.store.name })
        .subscribe( response => {
    });
  }

}

interface tiendas{
  texto:string;
}