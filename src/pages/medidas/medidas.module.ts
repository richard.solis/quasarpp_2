import { NgModule } from '@angular/core';
import { Medidas } from './medidas';

@NgModule({
  declarations: [
    Medidas,
  ],
  imports: [
  ],
  exports: [
    Medidas
  ]
})
export class MedidasModule {}
