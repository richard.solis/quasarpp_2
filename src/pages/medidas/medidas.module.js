var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { Medidas } from './medidas';
var MedidasModule = (function () {
    function MedidasModule() {
    }
    return MedidasModule;
}());
MedidasModule = __decorate([
    NgModule({
        declarations: [
            Medidas,
        ],
        imports: [],
        exports: [
            Medidas
        ]
    })
], MedidasModule);
export { MedidasModule };
//# sourceMappingURL=medidas.module.js.map