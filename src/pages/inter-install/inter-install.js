var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Platform, AlertController, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInstallService } from "../../services/tasksinstall.service";
import { TasksUnistallService } from "../../services/tasksunistall.service";
import { TasksMeasuService } from "../../services/tasksmeasurements.service";
import { TasksInventoryService } from "../../services/tasksinventary.service";
import { TasksIssue } from "../../services/tasksissue.service";
import { CMotiveService } from '../../services/campaignsmotive.service';
import { CSubMotiveService } from '../../services/campaignssubmotive.service';
import { CTargetService } from '../../services/campaignsissuetarget.service';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { CCAtegoriaService } from '../../services/campaignsissuecategoria.service';
import { CInstallStatusService } from '../../services/campaignsinstallstatus.service';
import { HomePage } from '../home/home';
import { FileUploader } from 'ng2-file-upload';
import { AppSettings } from '../../app.settings';
/**
 * Generated class for the InterInstallPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var InterInstall = (function () {
    function InterInstall(sessionS, tasksI, tasksU, tasksM, tasksIn, navCtrl, navParams, plt, camera, taskIs, CMotive, CSubMotive, CTarget, CStatus, CScategoria, CInstallStatus, alertCtrl, loadingCtrl) {
        this.sessionS = sessionS;
        this.tasksI = tasksI;
        this.tasksU = tasksU;
        this.tasksM = tasksM;
        this.tasksIn = tasksIn;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.plt = plt;
        this.camera = camera;
        this.taskIs = taskIs;
        this.CMotive = CMotive;
        this.CSubMotive = CSubMotive;
        this.CTarget = CTarget;
        this.CStatus = CStatus;
        this.CScategoria = CScategoria;
        this.CInstallStatus = CInstallStatus;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.uploader = new FileUploader({ url: AppSettings.BASE_PATH + AppSettings });
        this.dataSend = {
            campaign: '',
            code: '',
            store: '',
            file: '',
            comment: '',
        };
        this.dataSendIssue = {
            campaign: '',
            code: '',
            store: '',
            file: '',
            status: '',
            target: '',
            comment: '',
            motive: '',
            submotive: '',
            install_status: ''
        };
        this.image = '';
        this.type_task = '';
        this.motiveA = [];
        this.SubmotiveA = [];
        this.targetA = [];
        this.statusA = [];
        this.categoriaA = [];
        this.statusInstall = [];
        this.namecampaign = "";
        this.namestore = "";
        this.task_name = "";
        this.location_name = "";
    }
    InterInstall.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad InterInstall hh');
        this.type_task = this.sessionS.getItem('type_task');
        this.namecampaign = this.sessionS.getObject('install').name;
        this.namestore = this.sessionS.getObject('tienda').store.name;
        this.dataSend.campaign = this.sessionS.getObject('install')._id;
        this.dataSend.code = this.sessionS.getObject('install').code;
        this.dataSend.store = this.sessionS.getObject('tienda').store._id;
        this.dataSendIssue.campaign = this.sessionS.getObject('install')._id;
        this.dataSendIssue.code = this.sessionS.getObject('install').code;
        this.dataSendIssue.store = this.sessionS.getObject('tienda').store._id;
        this.dataSendIssue.target = 'Instalación';
        this.location_name = this.sessionS.getObject('install').location;
        this.CInstallStatus.getCIStatus()
            .subscribe(function (response) {
            if (_this.type_task == 'install') {
                _this.dataSendIssue.target = 'Instalación';
                _this.statusInstall = response.result;
            }
            else {
                _this.dataSendIssue.target = 'Desinstalación';
                _this.statusInstall = ['REALIZADO', 'NO REALIZADO'];
            }
            _this.CStatus.getIStatus()
                .subscribe(function (response) {
                console.log("status status");
                _this.statusA = response.result;
                _this.CTarget.getTarget()
                    .subscribe(function (response) {
                    _this.targetA = response.result;
                    _this.CMotive.getCMotive()
                        .subscribe(function (response) {
                        _this.motiveA = response.result;
                        _this.CSubMotive.getCSubMotive()
                            .subscribe(function (response) {
                            _this.SubmotiveA = response.result;
                        });
                    });
                });
            });
        });
    };
    InterInstall.prototype.funcionInstalado = function () {
        var _this = this;
        if (this.type_task == 'install') {
            this.tasksI.setInstall(this.dataSend)
                .subscribe(function (response) {
                console.log(response.result);
            });
        }
        else if (this.type_task == 'uninstall') {
            this.tasksU.setUninstall(this.dataSend)
                .subscribe(function (response) {
                console.log(response.result);
                _this.navCtrl.setRoot(HomePage);
            });
        }
        else if (this.type_task == 'measurements') {
            this.tasksM.setMeasurements(this.dataSend)
                .subscribe(function (response) {
                console.log(response.result);
            });
        }
        else if (this.type_task == 'inventory') {
            this.tasksIn.setInventory(this.dataSend)
                .subscribe(function (response) {
                console.log(response.result);
            });
        }
        this.navCtrl.setRoot(HomePage);
    };
    InterInstall.prototype.foto = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000,
                correctOrientation: true
            }).then(function (imageData) {
                _this.dataSend.file = imageData;
                _this.dataSendIssue.file = imageData;
                _this.image = "data:image/jpeg;base64," + imageData;
                console.log(_this.image);
            }, function (err) {
                console.log(err);
            });
        });
    };
    InterInstall.prototype.enviarInstalacion = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.taskIs.postTasksIssue(this.dataSend)
            .subscribe(function (response) {
            console.log(response.result);
            loader.dismiss();
            _this.navCtrl.setRoot(HomePage);
        });
    };
    return InterInstall;
}());
InterInstall = __decorate([
    IonicPage(),
    Component({
        selector: 'page-inter-install',
        templateUrl: 'inter-install.html',
        providers: [Camera]
    }),
    __metadata("design:paramtypes", [SessionService,
        TasksInstallService,
        TasksUnistallService,
        TasksMeasuService,
        TasksInventoryService,
        NavController,
        NavParams,
        Platform,
        Camera,
        TasksIssue,
        CMotiveService,
        CSubMotiveService,
        CTargetService,
        CStatus,
        CCAtegoriaService,
        CInstallStatusService,
        AlertController,
        LoadingController])
], InterInstall);
export { InterInstall };
//# sourceMappingURL=inter-install.js.map