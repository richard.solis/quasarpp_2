import { NgModule } from '@angular/core';
import { InterInstall } from './inter-install';

@NgModule({
  declarations: [
    InterInstall,
  ],
  imports: [
    
  ],
  exports: [
    InterInstall
  ]
})
export class InterInstallPageModule {}
