import { Component } from '@angular/core';
import { Platform, AlertController, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInstallService } from "../../services/tasksinstall.service";
import { TasksUnistallService } from "../../services/tasksunistall.service";
import { TasksMeasuService } from "../../services/tasksmeasurements.service";
import { TasksInventoryService } from "../../services/tasksinventary.service";
import { TasksIssue } from "../../services/tasksissue.service";
import { CMotiveService } from '../../services/campaignsmotive.service';
import { CSubMotiveService } from '../../services/campaignssubmotive.service';
import { CTargetService } from '../../services/campaignsissuetarget.service';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { CCAtegoriaService } from '../../services/campaignsissuecategoria.service';
import { CInstallStatusService } from '../../services/campaignsinstallstatus.service';
import { HomePage } from '../home/home';
import { FileSelectDirective, FileDropDirective, FileUploader, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { AppSettings } from '../../app.settings'; 

/**
 * Generated class for the InterInstallPage page. 
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-inter-install',
  templateUrl: 'inter-install.html',
  providers:[Camera] 
})
export class InterInstall {

  public uploader:FileUploader = new FileUploader({url: AppSettings.BASE_PATH + AppSettings});

	public dataSend = {
		campaign:'',
		code:'',
		store:'',
		file:'',
    comment:'',
	}
  public dataSendIssue = {
    campaign:'',
    code:'',
    store:'',
    file:'',
    status:'',
    target:'',
    comment:'',
    motive:'', 
    submotive: '',
    install_status: ''
  }

	public image = '';
	public type_task = '';
  constructor(public sessionS:SessionService, 
              public tasksI:TasksInstallService, 
              public tasksU:TasksUnistallService, 
              public tasksM:TasksMeasuService, 
              public tasksIn:TasksInventoryService, 
              public navCtrl: NavController, 
              public navParams: NavParams, 
              public plt: Platform, 
              public camera:Camera,
              public taskIs:TasksIssue,
              public CMotive:CMotiveService,
              public CSubMotive:CSubMotiveService,
              public CTarget:CTargetService,
              public CStatus:CStatus,
              public CScategoria:CCAtegoriaService,
              public CInstallStatus:CInstallStatusService,
              public alertCtrl:AlertController,
              public loadingCtrl: LoadingController){
  }
  public base64Image: string;
  motiveA=[];
  SubmotiveA=[];
  targetA=[];
  statusA=[];
  categoriaA=[];
  statusInstall=[];
  namecampaign = "";
  namestore = "";
  task_name = "";
  location_name = "";
  ionViewDidLoad() {
    console.log('ionViewDidLoad InterInstall hh');
    this.type_task = this.sessionS.getItem('type_task');
    this.namecampaign = this.sessionS.getObject('install').name;
    this.namestore = this.sessionS.getObject('tienda').store.name;
    this.dataSend.campaign = this.sessionS.getObject('install')._id;
    this.dataSend.code = this.sessionS.getObject('install').code;
    this.dataSend.store= this.sessionS.getObject('tienda').store._id;
    this.dataSendIssue.campaign = this.sessionS.getObject('install')._id;
    this.dataSendIssue.code = this.sessionS.getObject('install').code;
    this.dataSendIssue.store= this.sessionS.getObject('tienda').store._id;
    this.dataSendIssue.target= 'Instalación';
    this.location_name = this.sessionS.getObject('install').location;

    this.CInstallStatus.getCIStatus()
      .subscribe(response => {
        if(this.type_task == 'install'){
          this.dataSendIssue.target= 'Instalación';
          this.statusInstall=response.result;
        }
        else{
          this.dataSendIssue.target= 'Desinstalación';
          this.statusInstall = ['REALIZADO', 'NO REALIZADO'];
        }
        this.CStatus.getIStatus()
          .subscribe(response => {
            console.log("status status");
            this.statusA=response.result;
            this.CTarget.getTarget()
              .subscribe(response => {
                this.targetA=response.result;
                this.CMotive.getCMotive()
                  .subscribe(response => {
                    this.motiveA=response.result;
                    this.CSubMotive.getCSubMotive()
                      .subscribe(response => {
                        this.SubmotiveA=response.result;
                     });
                  }); 
              }); 
          }); 
      }); 
    
  }

  funcionInstalado(){ 

    if(this.type_task == 'install'){
      this.tasksI.setInstall(this.dataSend)
        .subscribe(response => {
          console.log(response.result);
      }); 
    }
    else if(this.type_task == 'uninstall'){
      this.tasksU.setUninstall(this.dataSend)
        .subscribe(response => {
          console.log(response.result);
          this.navCtrl.setRoot(HomePage)
      }); 
    }
    else if(this.type_task == 'measurements'){
      this.tasksM.setMeasurements(this.dataSend)
        .subscribe(response => {
          console.log(response.result);
      }); 
    }
    else if(this.type_task == 'inventory'){
      this.tasksIn.setInventory(this.dataSend)
        .subscribe(response => {
          console.log(response.result);
      }); 
    }
    this.navCtrl.setRoot(HomePage);
   
  }

  foto(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation:true
    }).then((imageData) => {
        this.dataSend.file = imageData;
        this.dataSendIssue.file = imageData;
        this.image = "data:image/jpeg;base64," + imageData;
        console.log(this.image)
 
    }, (err) => {
        console.log(err);
    	});
    });
  }



  enviarInstalacion(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.taskIs.postTasksIssue(this.dataSend)
      .subscribe(response => {
        console.log(response.result);
        loader.dismiss();
        this.navCtrl.setRoot(HomePage)
      });
      
      
  }

}
