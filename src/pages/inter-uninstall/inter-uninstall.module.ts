import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { InterUninstallPage } from './inter-uninstall';

@NgModule({
  declarations: [
    InterUninstallPage,
  ],
  imports: [
  ],
  exports: [
    InterUninstallPage
  ]
})
export class InterUninstallPageModule {}
