import { NgModule } from '@angular/core';
import { Ubicacion } from './ubicacion';

@NgModule({
  declarations: [
    Ubicacion,
  ],
  imports: [
  ],
  exports: [
    Ubicacion
  ]
})
export class UbicacionModule {}
