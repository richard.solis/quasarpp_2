var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { SessionService } from "../../services/session.service";
import { PTasksConfirm } from '../../services/tasksconfirmpost.service';
import { HomePage } from '../home/home';
/**
 * Generated class for the ConfirmInterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ConfirmInterPage = (function () {
    function ConfirmInterPage(navCtrl, navParams, plt, camera, sessionS, loadingCtrl, alertCtrl, Pconfirm) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.plt = plt;
        this.camera = camera;
        this.sessionS = sessionS;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.Pconfirm = Pconfirm;
        this.DataConfirm = {
            campaign: '',
            code: '',
            store: '',
            file: '',
            reference: '',
            comment: ''
        };
        this.image = '';
    }
    ConfirmInterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConfirmInterPage');
        this.Store = this.sessionS.getObject('confirmaData').store.name;
        this.nameCampana = this.sessionS.getObject('confirmaData').name;
        this.DataConfirm.campaign = this.sessionS.getObject('confirmaData')._id;
        this.DataConfirm.code = this.sessionS.getObject('confirmaData').code;
        this.DataConfirm.store = this.sessionS.getObject('tienda').store._id;
        this.DataConfirm.comment = '';
        this.DataConfirm.reference = '';
        console.log(this.sessionS.getObject('confirmaData').name);
    };
    ConfirmInterPage.prototype.foto = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000,
                correctOrientation: true
            }).then(function (imageData) {
                _this.DataConfirm.file = imageData;
                _this.image = "data:image/jpeg;base64," + imageData;
                console.log(_this.image);
            }, function (err) {
                console.log(err);
            });
        });
    };
    ConfirmInterPage.prototype.enviarConfirm = function () {
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.Pconfirm.postTConfirm(this.DataConfirm)
            .subscribe(function (response) {
            console.log(response.result);
            loader.dismiss();
        });
        this.navCtrl.setRoot(HomePage);
    };
    return ConfirmInterPage;
}());
ConfirmInterPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-confirm-inter',
        templateUrl: 'confirm-inter.html',
        providers: [Camera],
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        Platform,
        Camera,
        SessionService,
        LoadingController,
        AlertController,
        PTasksConfirm])
], ConfirmInterPage);
export { ConfirmInterPage };
//# sourceMappingURL=confirm-inter.js.map