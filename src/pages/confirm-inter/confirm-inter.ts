import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { SessionService } from "../../services/session.service";
import { PTasksConfirm } from '../../services/tasksconfirmpost.service';
import { HomePage } from '../home/home';
 
/**
 * Generated class for the ConfirmInterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-confirm-inter',
  templateUrl: 'confirm-inter.html', 
  providers:[Camera],
})
export class ConfirmInterPage {

public DataConfirm={
  campaign:'',
  code:'',
  store:'',
  file:'',
  reference:'',
  comment:''
}
public image = '';
  constructor(public navCtrl: NavController,
  			      public navParams: NavParams,
  			      public plt: Platform,
          	  public camera:Camera,
          	  public sessionS:SessionService,
          	  public loadingCtrl: LoadingController,
         	    public alertCtrl:AlertController,
         	    public Pconfirm:PTasksConfirm) {
  }
 public Store;
 public nameCampana;
 public base64Image: string;
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmInterPage');

    this.Store = this.sessionS.getObject('confirmaData').store.name;
    this.nameCampana = this.sessionS.getObject('confirmaData').name;
    this.DataConfirm.campaign = this.sessionS.getObject('confirmaData')._id;
    this.DataConfirm.code=this.sessionS.getObject('confirmaData').code;
    this.DataConfirm.store=this.sessionS.getObject('tienda').store._id;
    this.DataConfirm.comment='';
    this.DataConfirm.reference='';

    console.log(this.sessionS.getObject('confirmaData').name);


  }
  foto(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation:true
    }).then((imageData) => {
        this.DataConfirm.file = imageData;
        this.image = "data:image/jpeg;base64," + imageData;
        console.log(this.image)        
    }, (err) => {
        console.log(err);
      });
    });
  }
  enviarConfirm(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.Pconfirm.postTConfirm(this.DataConfirm)
      .subscribe(response => {
        console.log(response.result);
        loader.dismiss();  
      });
     this.navCtrl.setRoot(HomePage);
  }

}
