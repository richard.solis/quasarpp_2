import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ConfirmInterPage } from './confirm-inter';

@NgModule({
  declarations: [
    ConfirmInterPage,
  ],
  imports: [
  ],
  exports: [
    ConfirmInterPage
  ]
})
export class ConfirmInterPageModule {}
