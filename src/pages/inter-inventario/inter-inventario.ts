import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInstallService } from "../../services/tasksinstall.service";
import { TasksUnistallService } from "../../services/tasksunistall.service";
import { TasksMeasuService } from "../../services/tasksmeasurements.service";
import { TasksInventoryService } from "../../services/tasksinventary.service";
import { TasksIssue } from "../../services/tasksissue.service";
import { CMotiveService } from '../../services/campaignsmotive.service';
import { CSubMotiveService } from '../../services/campaignssubmotive.service';
import { CTargetService } from '../../services/campaignsissuetarget.service';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { CCAtegoriaService } from '../../services/campaignsissuecategoria.service';
import { CInstallStatusService } from '../../services/campaignsinstallstatus.service';
import { Incidencia } from '../incidencia/incidencia';
import { CImplementers } from '../../services/campaignsimplementers.service';
import { TasksStoreService } from "../../services/tasksstore.service";
import { PTasksInventory } from '../../services/tasksinventorypost.service';
import { HomePage } from '../home/home';

/**
 * Generated class for the InterInventario page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage() 
@Component({
  selector: 'page-inter-inventario',
  templateUrl: 'inter-inventario.html',
  providers:[Camera] 
})
export class InterInventario {
  public dataSend = {
    campaign:'',
    code:'',
    store:'',
    file:'',
    implementer:'Quasar',
    comment:'',
  }
  public dataInstall={
    motive:'',
    submotive: '',
    campaign:'',
    code:'',
    store:'',
    file:'',
    comment:'',

  }
  public image = '';
  public DatosOtros={
    motivo:'Elemento no instalado por Quasar',
    submotivo:'Instalado por proveedor no autorizado',
  };
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public plt: Platform,
              public camera:Camera,
              public taskIs:TasksIssue,
              public CMotive:CMotiveService,
              public CSubMotive:CSubMotiveService,
              public CTarget:CTargetService,
              public CStatus:CStatus,
              public sessionS:SessionService,
              public CScategoria:CCAtegoriaService,
              public CInstallStatus:CInstallStatusService,
              public alertCtrl:AlertController,
              public camImplementers:CImplementers,
              public tasksS:TasksStoreService, 
              public PTInventory:PTasksInventory,
              public loadingCtrl: LoadingController) {
  }
  public base64Image: string;
  public Store;

  implements=[];
  motiveA=[];
  SubmotiveA=[];
  targetA=[];
  statusInstall=[]; 
  ionViewDidLoad() {
    console.log('ionViewDidLoad InterInventario');
    this.Store = this.sessionS.getObject('install').name;
    this.dataSend.campaign = this.sessionS.getObject('install')._id;
    this.dataSend.code = this.sessionS.getObject('install').code;
    this.dataSend.store = this.sessionS.getObject('tienda').store._id;
    this.dataInstall.campaign = this.sessionS.getObject('install')._id;
    this.dataInstall.code = this.sessionS.getObject('install').code;
    this.dataInstall.store = this.sessionS.getObject('tienda').store._id;
    this.dataInstall.motive= this.DatosOtros.motivo;
    this.dataInstall.submotive= this.DatosOtros.submotivo;
    this.dataInstall.comment='';

     this.camImplementers.getCImplementers()
     .subscribe(response => {
        this.implements=response.result;
          console.log(this.implements);
     }); 
     this.CMotive.getCMotive()
      .subscribe(response => {
        this.motiveA=response.result;
        console.log(this.motiveA)
      }); 
    this.CSubMotive.getCSubMotive()
      .subscribe(response => {
        this.SubmotiveA=response.result;
        console.log(this.SubmotiveA)
     });
      this.CTarget.getTarget()
      .subscribe(response => {
        this.targetA=response.result;
        console.log(this.targetA)
      }); 
     this.CInstallStatus.getCIStatus()
      .subscribe(response => {
        this.statusInstall=response.result;
        console.log(this.statusInstall)
     }); 
     
                        
  }
  
  foto(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation:true
    }).then((imageData) => {
        this.dataSend.file = imageData;
         this.dataInstall.file=imageData;
        this.image = "data:image/jpeg;base64," + imageData;
        console.log(this.image)        
    }, (err) => {
        console.log(err);
      });
    });
     console.log(this.DatosOtros.motivo);
  } 

  EnviarInventarioOk(){
    let loader = this.loadingCtrl.create();
    loader.present();
    this.PTInventory.postTInventory(this.dataSend)
      .subscribe(response => {
        loader.dismiss();
        this.navCtrl.setRoot(HomePage);
     });
  }

  EnviarInventarioQuasar(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.taskIs.postTasksIssue(this.dataInstall)
      .subscribe(response => {
        loader.dismiss();
        this.navCtrl.setRoot(HomePage);
     });
  }



} 
