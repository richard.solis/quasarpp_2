var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksIssue } from "../../services/tasksissue.service";
import { CMotiveService } from '../../services/campaignsmotive.service';
import { CSubMotiveService } from '../../services/campaignssubmotive.service';
import { CTargetService } from '../../services/campaignsissuetarget.service';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { CCAtegoriaService } from '../../services/campaignsissuecategoria.service';
import { CInstallStatusService } from '../../services/campaignsinstallstatus.service';
import { CImplementers } from '../../services/campaignsimplementers.service';
import { TasksStoreService } from "../../services/tasksstore.service";
import { PTasksInventory } from '../../services/tasksinventorypost.service';
import { HomePage } from '../home/home';
/**
 * Generated class for the InterInventario page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var InterInventario = (function () {
    function InterInventario(navCtrl, navParams, plt, camera, taskIs, CMotive, CSubMotive, CTarget, CStatus, sessionS, CScategoria, CInstallStatus, alertCtrl, camImplementers, tasksS, PTInventory, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.plt = plt;
        this.camera = camera;
        this.taskIs = taskIs;
        this.CMotive = CMotive;
        this.CSubMotive = CSubMotive;
        this.CTarget = CTarget;
        this.CStatus = CStatus;
        this.sessionS = sessionS;
        this.CScategoria = CScategoria;
        this.CInstallStatus = CInstallStatus;
        this.alertCtrl = alertCtrl;
        this.camImplementers = camImplementers;
        this.tasksS = tasksS;
        this.PTInventory = PTInventory;
        this.loadingCtrl = loadingCtrl;
        this.dataSend = {
            campaign: '',
            code: '',
            store: '',
            file: '',
            implementer: 'Quasar',
            comment: '',
        };
        this.dataInstall = {
            motive: '',
            submotive: '',
            campaign: '',
            code: '',
            store: '',
            file: '',
            comment: '',
            categoria: ''
        };
        this.image = '';
        this.DatosOtros = {
            motivo: 'Elemento no instalado por Quasar',
            submotivo: 'Instalado por proveedor no autorizado',
        };
        this.implements = [];
        this.motiveA = [];
        this.SubmotiveA = [];
        this.targetA = [];
        this.statusInstall = [];
    }
    InterInventario.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad InterInventario');
        this.Store = this.sessionS.getObject('tienda').store.name;
        this.dataSend.campaign = this.sessionS.getObject('install')._id;
        this.dataSend.code = this.sessionS.getObject('install').code;
        this.dataSend.store = this.sessionS.getObject('tienda').store._id;
        this.dataInstall.campaign = this.sessionS.getObject('install')._id;
        this.dataInstall.code = this.sessionS.getObject('install').code;
        this.dataInstall.store = this.sessionS.getObject('tienda').store._id;
        this.dataInstall.motive = this.DatosOtros.motivo;
        this.dataInstall.submotive = this.DatosOtros.submotivo;
        this.dataInstall.comment = '';
        this.camImplementers.getCImplementers()
            .subscribe(function (response) {
            _this.implements = response.result;
            console.log(_this.implements);
        });
        this.CMotive.getCMotive()
            .subscribe(function (response) {
            _this.motiveA = response.result;
            console.log(_this.motiveA);
        });
        this.CSubMotive.getCSubMotive()
            .subscribe(function (response) {
            _this.SubmotiveA = response.result;
            console.log(_this.SubmotiveA);
        });
        this.CTarget.getTarget()
            .subscribe(function (response) {
            _this.targetA = response.result;
            console.log(_this.targetA);
        });
        this.CInstallStatus.getCIStatus()
            .subscribe(function (response) {
            _this.statusInstall = response.result;
            console.log(_this.statusInstall);
        });
    };
    InterInventario.prototype.foto = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000,
                correctOrientation: true
            }).then(function (imageData) {
                _this.dataSend.file = imageData;
                _this.dataInstall.file = imageData;
                _this.image = "data:image/jpeg;base64," + imageData;
                console.log(_this.image);
            }, function (err) {
                console.log(err);
            });
        });
        console.log(this.DatosOtros.motivo);
    };
    InterInventario.prototype.EnviarInventarioOk = function () {
        var _this = this;
        var loader = this.loadingCtrl.create();
        loader.present();
        this.PTInventory.postTInventory(this.dataSend)
            .subscribe(function (response) {
            loader.dismiss();
            _this.navCtrl.setRoot(HomePage);
        });
    };
    InterInventario.prototype.EnviarInventarioQuasar = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.taskIs.postTasksIssue(this.dataInstall)
            .subscribe(function (response) {
            loader.dismiss();
            _this.navCtrl.setRoot(HomePage);
        });
    };
    return InterInventario;
}());
InterInventario = __decorate([
    IonicPage(),
    Component({
        selector: 'page-inter-inventario',
        templateUrl: 'inter-inventario.html',
        providers: [Camera]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        Platform,
        Camera,
        TasksIssue,
        CMotiveService,
        CSubMotiveService,
        CTargetService,
        CStatus,
        SessionService,
        CCAtegoriaService,
        CInstallStatusService,
        AlertController,
        CImplementers,
        TasksStoreService,
        PTasksInventory,
        LoadingController])
], InterInventario);
export { InterInventario };
//# sourceMappingURL=inter-inventario.js.map