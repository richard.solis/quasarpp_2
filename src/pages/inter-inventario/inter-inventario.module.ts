import { NgModule } from '@angular/core';
import { InterInventario } from './inter-inventario';

@NgModule({
  declarations: [
    InterInventario,
  ],
  imports: [
    
  ],
  exports: [
    InterInventario
  ]
})
export class InterInventarioModule {}
