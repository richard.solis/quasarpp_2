import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Tarea page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tarea',
  templateUrl: 'tarea.html',
})
export class Tarea {
  tareaVista:Array<tarea>=[
    {tareas:'Instalaciones',
     cantidad:'(3/3)',
     iconoTarea:'ios-checkmark-circle-outline'},

     {tareas:'Inventario',
     cantidad:'(3/15)',
     iconoTarea:'md-alert'},

     {tareas:'Mantenimiento',
     cantidad:'',
     iconoTarea:'md-alert'},

     {tareas:'Relevamiento de info',
     cantidad:'(6/7)',
     iconoTarea:'md-alert'},

     {tareas:'Confirmacion de espacios',
     cantidad:'(2/2)',
     iconoTarea:'ios-checkmark-circle-outline'},

     {tareas:'Toma de medidas',
     cantidad:'(5/5)',
     iconoTarea:'ios-checkmark-circle-outline'}

  ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
interface tarea{
  tareas:string;
  cantidad:string;
  iconoTarea:string;
}
