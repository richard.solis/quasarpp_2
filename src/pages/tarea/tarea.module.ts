import { NgModule } from '@angular/core';
import { Tarea } from './tarea';

@NgModule({
  declarations: [
    Tarea,
  ],
  imports: [
    
  ],
  exports: [
    Tarea
  ]
})
export class TareaModule {}
