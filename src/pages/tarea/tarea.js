var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the Tarea page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Tarea = (function () {
    function Tarea(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tareaVista = [
            { tareas: 'Instalaciones',
                cantidad: '(3/3)',
                iconoTarea: 'ios-checkmark-circle-outline' },
            { tareas: 'Inventario',
                cantidad: '(3/15)',
                iconoTarea: 'md-alert' },
            { tareas: 'Mantenimiento',
                cantidad: '',
                iconoTarea: 'md-alert' },
            { tareas: 'Relevamiento de info',
                cantidad: '(6/7)',
                iconoTarea: 'md-alert' },
            { tareas: 'Confirmacion de espacios',
                cantidad: '(2/2)',
                iconoTarea: 'ios-checkmark-circle-outline' },
            { tareas: 'Toma de medidas',
                cantidad: '(5/5)',
                iconoTarea: 'ios-checkmark-circle-outline' }
        ];
    }
    return Tarea;
}());
Tarea = __decorate([
    IonicPage(),
    Component({
        selector: 'page-tarea',
        templateUrl: 'tarea.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams])
], Tarea);
export { Tarea };
//# sourceMappingURL=tarea.js.map