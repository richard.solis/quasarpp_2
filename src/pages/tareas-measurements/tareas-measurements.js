var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksMeasuService } from "../../services/tasksmeasurements.service";
import { MeasurementsInterPage } from '../measurements-inter/measurements-inter';
import { TasksPage } from '../tasks/tasks';
/**
 * Generated class for the TareasMeasurementsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TareasMeasurementsPage = (function () {
    function TareasMeasurementsPage(navCtrl, navParams, sessionS, tasksM, loadingCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sessionS = sessionS;
        this.tasksM = tasksM;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.measurements = [];
    }
    TareasMeasurementsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(TasksPage);
        });
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.sessionS.setItem('type_task', 'measurements');
        this.tasksM.getTasksMeasurements(this.sessionS.getObject('tienda').store._id)
            .subscribe(function (response) {
            _this.measurements = response.result;
            console.log(_this.measurements);
            loader.dismiss();
        });
    };
    TareasMeasurementsPage.prototype.interMeasurement = function (item) {
        this.sessionS.setObject('dataMedida', item);
        this.navCtrl.push(MeasurementsInterPage);
    };
    return TareasMeasurementsPage;
}());
TareasMeasurementsPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-tareas-measurements',
        templateUrl: 'tareas-measurements.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        SessionService,
        TasksMeasuService,
        LoadingController,
        Platform])
], TareasMeasurementsPage);
export { TareasMeasurementsPage };
//# sourceMappingURL=tareas-measurements.js.map