import { NgModule } from '@angular/core';
import { TareasMeasurementsPage } from './tareas-measurements';

@NgModule({
  declarations: [
    TareasMeasurementsPage,
  ],
  imports: [
    
  ],
  exports: [
    TareasMeasurementsPage
  ]
})
export class TareasMeasurementsPageModule {}
