import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksMeasuService } from "../../services/tasksmeasurements.service";
import { MeasurementsInterPage } from '../measurements-inter/measurements-inter';
import { TasksPage } from '../tasks/tasks';
import { touchRote } from '../../services/touchrote.service';

/**
 * Generated class for the TareasMeasurementsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tareas-measurements',
  templateUrl: 'tareas-measurements.html',
})
export class TareasMeasurementsPage {

  measurements=[];

  constructor(public navCtrl: NavController,
  			  public navParams: NavParams,
  			  public sessionS:SessionService,
  			  public tasksM:TasksMeasuService,
          public loadingCtrl: LoadingController,
          public platform:Platform,
          public touch: touchRote) {
  }

  ionViewDidLoad() {
    this.platform.registerBackButtonAction(() => {
        this.navCtrl.setRoot(TasksPage); 
      });
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.sessionS.setItem('type_task', 'measurements');
    this.tasksM.getTasksMeasurements(this.sessionS.getObject('tienda').store._id)
      .subscribe(response => {
        this.measurements=response.result;
      	console.log(this.measurements);
        loader.dismiss()
      }); 
  }
  interMeasurement(item){
    this.sessionS.setObject('dataMedida',item)
    this.navCtrl.push(MeasurementsInterPage);
    this.touch.postTouch({path: 'Home/' + this.sessionS.getObject('tienda').store.name + '/' +  this.sessionS.getItem('items_task') + '/' + item.name })
        .subscribe( response => {
    }); 
  }

}
