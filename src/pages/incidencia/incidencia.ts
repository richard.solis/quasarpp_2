import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Modal } from 'ionic-angular';
import { SessionService } from '../../services/session.service';
import { Contacto } from '../contacto/contacto';

/**
 * Generated class for the Incidencia page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-incidencia',
  templateUrl: 'incidencia.html', 
  providers:[SessionService]

})


export class Incidencia { 
  btnMaps:Array<mapSearch>=[
  { Xlongi:-12.090534, Ylatitud: -77.022782, name:"lugar 1" },
  { Xlongi:-12.088148, Ylatitud: -77.011516, name:"lugar 2" },
  { Xlongi:-12.088179, Ylatitud: -77.014048, name:"lugar 3" },
  { Xlongi:-12.087403, Ylatitud: -77.012793, name:"lugar 4" },
];

  constructor(public modalCtrl: ModalController,
              public navCtrl: NavController,
              public params: NavParams,
              private sesionS:SessionService) {

              }


  next(data){
    this.navCtrl.push(Contacto); 
    this.sesionS.setObject('mapaLocal',data);
    console.log(data.Xlongi , data.Ylatitud);
  }



}

interface mapSearch{
    Xlongi:number;
    Ylatitud:number;
    name:string;
 }
