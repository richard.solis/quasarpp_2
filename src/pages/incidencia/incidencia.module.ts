import { NgModule } from '@angular/core';
import { Incidencia } from './incidencia';

@NgModule({
  declarations: [
    Incidencia,
  ],
  imports: [
  ],
  exports: [
    Incidencia
  ]
})
export class IncidenciaModule {}
