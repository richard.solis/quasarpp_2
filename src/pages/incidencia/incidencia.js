var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SessionService } from '../../services/session.service';
import { Contacto } from '../contacto/contacto';
/**
 * Generated class for the Incidencia page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Incidencia = (function () {
    function Incidencia(modalCtrl, navCtrl, params, sesionS) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.params = params;
        this.sesionS = sesionS;
        this.btnMaps = [
            { Xlongi: -12.090534, Ylatitud: -77.022782, name: "lugar 1" },
            { Xlongi: -12.088148, Ylatitud: -77.011516, name: "lugar 2" },
            { Xlongi: -12.088179, Ylatitud: -77.014048, name: "lugar 3" },
            { Xlongi: -12.087403, Ylatitud: -77.012793, name: "lugar 4" },
        ];
    }
    Incidencia.prototype.next = function (data) {
        this.navCtrl.push(Contacto);
        this.sesionS.setObject('mapaLocal', data);
        console.log(data.Xlongi, data.Ylatitud);
    };
    return Incidencia;
}());
Incidencia = __decorate([
    IonicPage(),
    Component({
        selector: 'page-incidencia',
        templateUrl: 'incidencia.html',
        providers: [SessionService]
    }),
    __metadata("design:paramtypes", [ModalController,
        NavController,
        NavParams,
        SessionService])
], Incidencia);
export { Incidencia };
//# sourceMappingURL=incidencia.js.map