var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
/**
 * Generated class for the Instalacion page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Instalacion = (function () {
    function Instalacion(navCtrl, navParams, geolocation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.geolocation = geolocation;
    }
    Instalacion.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Instalacion');
    };
    Instalacion.prototype.locateUser = function () {
        var _this = this;
        this.geolocation.getCurrentPosition()
            .then(function (location) {
            console.log("Location");
            _this.location = location;
        })
            .catch(function (error) { return console.log("Error"); });
    };
    return Instalacion;
}());
Instalacion = __decorate([
    IonicPage(),
    Component({
        selector: 'page-instalacion',
        templateUrl: 'instalacion.html',
        providers: [Geolocation],
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        Geolocation])
], Instalacion);
export { Instalacion };
//# sourceMappingURL=instalacion.js.map