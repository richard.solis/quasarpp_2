import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation'

/**
 * Generated class for the Instalacion page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-instalacion',
  templateUrl: 'instalacion.html',
  providers:[Geolocation],
})
export class Instalacion {
  location: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public geolocation:Geolocation)
              {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad Instalacion');
  }

  locateUser(){
    this.geolocation.getCurrentPosition()
    .then(
      (location) => {
        console.log("Location");
        this.location = location;
      }
    )
    .catch(
      (error) => console.log("Error")
    )
  }
  

}
