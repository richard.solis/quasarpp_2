import { NgModule } from '@angular/core';
import { Instalacion } from './instalacion';

@NgModule({
  declarations: [
    Instalacion,
  ],
  imports: [

  ],
  exports: [
    Instalacion
  ]
})
export class InstalacionModule {}
