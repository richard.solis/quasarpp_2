import { Component } from '@angular/core';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams, Platform,LoadingController } from 'ionic-angular';
import { OpportunitiesS } from '../../services/opportunitiespost.service';
import { SessionService } from "../../services/session.service";
import { Oportunidades } from '../../services/opportunities.service';
import { HomePage } from '../home/home';


/**
 * Generated class for the Oportunidad page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-oportunidad',
  templateUrl: 'oportunidad.html',
  providers:[Camera]
})
export class Oportunidad {
  public dateOpportunities = {
    store:'',
    file:'',
    comment:'',
    type:''
  }
     public image = '';

     public Oportuni=[];
  constructor(public navCtrl: NavController,
              public navParams: NavParams, 
              public plt:Platform,
              public opportunitie:OpportunitiesS,
              public sessionS:SessionService,
              public camera:Camera,
              public opportunitieget:Oportunidades,
              public loadingCtrl: LoadingController){

              }
  public namestore = '';
ionViewDidLoad() {
    console.log('ionViewDidLoad InterInstall');

    this.namestore = this.sessionS.getObject('tienda').store.name;
    this.dateOpportunities.store= this.sessionS.getObject('tienda').store._id;

    this.opportunitieget.getOportunidades()
     .subscribe(response => {
        this.Oportuni=response.result;
          console.log(this.Oportuni);
     }); 

  }

  foto(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation:true
    }).then((imageData) => {
        this.dateOpportunities.file = imageData;
        this.image = "data:image/jpeg;base64," + imageData;
        console.log(this.image)
 
    }, (err) => {
        console.log(err);
      });
    });
  }

  opportunitiesEnviar(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.opportunitie.postOpportunities(this.dateOpportunities)
      .subscribe(response => {
        console.log(response.result);
        loader.dismiss();
        this.navCtrl.setRoot(HomePage);
     });
  }




}
  
  
  
 
    
 


