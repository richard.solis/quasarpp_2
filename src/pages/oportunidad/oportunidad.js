var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams, Platform, LoadingController } from 'ionic-angular';
import { OpportunitiesS } from '../../services/opportunitiespost.service';
import { SessionService } from "../../services/session.service";
import { Oportunidades } from '../../services/opportunities.service';
import { HomePage } from '../home/home';
/**
 * Generated class for the Oportunidad page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Oportunidad = (function () {
    function Oportunidad(navCtrl, navParams, plt, opportunitie, sessionS, camera, opportunitieget, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.plt = plt;
        this.opportunitie = opportunitie;
        this.sessionS = sessionS;
        this.camera = camera;
        this.opportunitieget = opportunitieget;
        this.loadingCtrl = loadingCtrl;
        this.dateOpportunities = {
            store: '',
            file: '',
            comment: '',
            type: ''
        };
        this.image = '';
        this.Oportuni = [];
        this.namestore = '';
    }
    Oportunidad.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad InterInstall');
        this.namestore = this.sessionS.getObject('tienda').store.name;
        this.dateOpportunities.store = this.sessionS.getObject('tienda').store._id;
        this.opportunitieget.getOportunidades()
            .subscribe(function (response) {
            _this.Oportuni = response.result;
            console.log(_this.Oportuni);
        });
    };
    Oportunidad.prototype.foto = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000,
                correctOrientation: true
            }).then(function (imageData) {
                _this.dateOpportunities.file = imageData;
                _this.image = "data:image/jpeg;base64," + imageData;
                console.log(_this.image);
            }, function (err) {
                console.log(err);
            });
        });
    };
    Oportunidad.prototype.opportunitiesEnviar = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.opportunitie.postOpportunities(this.dateOpportunities)
            .subscribe(function (response) {
            console.log(response.result);
            loader.dismiss();
            _this.navCtrl.setRoot(HomePage);
        });
    };
    return Oportunidad;
}());
Oportunidad = __decorate([
    IonicPage(),
    Component({
        selector: 'page-oportunidad',
        templateUrl: 'oportunidad.html',
        providers: [Camera]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        Platform,
        OpportunitiesS,
        SessionService,
        Camera,
        Oportunidades,
        LoadingController])
], Oportunidad);
export { Oportunidad };
//# sourceMappingURL=oportunidad.js.map