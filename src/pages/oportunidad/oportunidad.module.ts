import { NgModule } from '@angular/core';
import { Oportunidad } from './oportunidad';

@NgModule({
  declarations: [
    Oportunidad,
  ],
  imports: [
  ],
  exports: [
    Oportunidad
  ]
})
export class OportunidadModule {}
