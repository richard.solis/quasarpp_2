var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, LoadingController } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { Camera } from '@ionic-native/camera';
import { PTasksMeasurement } from '../../services/tasksmeasurementpost.service';
import { CMotiveService } from '../../services/campaignsmotive.service';
import { CSubMotiveService } from '../../services/campaignssubmotive.service';
import { CTargetService } from '../../services/campaignsissuetarget.service';
import { CInstallStatusService } from '../../services/campaignsinstallstatus.service';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { TasksIssue } from "../../services/tasksissue.service";
import { HomePage } from '../home/home';
import { TasksInventoryService } from '../../services/tasksinventary.service';
/**
 * Generated class for the MeasurementsInterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MeasurementsInterPage = (function () {
    function MeasurementsInterPage(navCtrl, navParams, sessionS, TMeasurement, plt, taskIs, camera, alertCtrl, CStatus, CInstallStatus, CSubMotive, CTarget, CMotive, loadingCtrl, TInventoryS) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sessionS = sessionS;
        this.TMeasurement = TMeasurement;
        this.plt = plt;
        this.taskIs = taskIs;
        this.camera = camera;
        this.alertCtrl = alertCtrl;
        this.CStatus = CStatus;
        this.CInstallStatus = CInstallStatus;
        this.CSubMotive = CSubMotive;
        this.CTarget = CTarget;
        this.CMotive = CMotive;
        this.loadingCtrl = loadingCtrl;
        this.TInventoryS = TInventoryS;
        this.image = '';
        this.medidasGeneral = {
            side: 'Lateral',
            value: '',
            quantity: null
        };
        this.medidasGeneral2 = {
            side: 'Frontal inferior',
            value: '',
            quantity: null
        };
        this.medidasGeneral3 = {
            side: 'Frontal Superior',
            value: '',
            quantity: null
        };
        this.medidasGeneral4 = {
            side: 'Flejeras',
            value: '',
            quantity: null
        };
        this.dataMeasurements = {
            campaign: '',
            code: '',
            store: '',
            file: '',
            observation: '',
            measurements: []
        };
        this.dataInstall = {
            motive: '',
            submotive: '',
            campaign: '',
            code: '',
            store: '',
            file: '',
            comment: '',
            target: '',
            install_status: ''
        };
        this.element = {
            name: '',
        };
        this.typesElement = {
            types: [],
        };
        this.implements = [];
        this.motiveA = [];
        this.SubmotiveA = [];
        this.targetA = [];
        this.statusInstall = [];
    }
    MeasurementsInterPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad MeasurementsInterPage');
        this.nameMedida = this.sessionS.getObject('dataMedida').element;
        this.Namecampaign = this.sessionS.getObject('dataMedida').name;
        this.dataMeasurements.campaign = this.sessionS.getObject('dataMedida')._id;
        this.dataMeasurements.code = this.sessionS.getObject('dataMedida').store.code;
        this.dataMeasurements.store = this.sessionS.getObject('tienda').store._id;
        this.Store = this.sessionS.getObject('tienda').store.name;
        this.dataMeasurements.observation = '';
        this.dataInstall.campaign = this.sessionS.getObject('dataMedida')._id;
        this.dataInstall.code = this.sessionS.getObject('dataMedida').store.code;
        this.dataInstall.store = this.sessionS.getObject('tienda').store._id;
        this.dataInstall.motive = '';
        this.dataInstall.submotive = '';
        this.dataInstall.comment = '';
        this.dataInstall.target = '';
        this.dataInstall.install_status = '';
        console.log(this.sessionS.getObject('dataMedida').store.code);
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.CMotive.getCMotive()
            .subscribe(function (response) {
            _this.motiveA = response.result;
            console.log(_this.motiveA);
        });
        this.CSubMotive.getCSubMotive()
            .subscribe(function (response) {
            _this.SubmotiveA = response.result;
            console.log(_this.SubmotiveA);
        });
        this.CTarget.getTarget()
            .subscribe(function (response) {
            _this.targetA = response.result;
            console.log(_this.targetA);
        });
        this.CInstallStatus.getCIStatus()
            .subscribe(function (response) {
            _this.statusInstall = response.result;
            console.log(_this.statusInstall);
            _this.TMeasurement.getMedidasCode(_this.dataMeasurements.code, _this.dataMeasurements.store)
                .subscribe(function (response) {
                console.log(response.result);
                loader.dismiss();
                _this.element = {
                    name: response.elementType,
                };
                _this.typesElement = response.element;
                _this.element = Object.assign({}, (_this.typesElement.types.filter(function (element) { return element.name == _this.element.name; })[0]));
                _this.dataMeasurements.measurements = response.result;
            });
        });
    };
    MeasurementsInterPage.prototype.foto = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000,
                correctOrientation: true
            }).then(function (imageData) {
                _this.dataMeasurements.file = imageData;
                _this.dataInstall.file = imageData;
                _this.image = "data:image/jpeg;base64," + imageData;
                console.log(_this.image);
            }, function (err) {
                console.log(err);
            });
        });
        console.log(this.dataMeasurements.campaign);
    };
    MeasurementsInterPage.prototype.enviarMeasurement = function () {
        var _this = this;
        console.log(this.dataMeasurements.measurements);
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.TMeasurement.postTMeasurement(this.dataMeasurements)
            .subscribe(function (response) {
            loader.dismiss();
            _this.navCtrl.setRoot(HomePage);
            console.log(response.result);
        });
    };
    MeasurementsInterPage.prototype.enviarMeasurementIssue = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.taskIs.postTasksIssue(this.dataInstall)
            .subscribe(function (response) {
            console.log(response.result);
            loader.dismiss();
            _this.navCtrl.setRoot(HomePage);
        });
    };
    MeasurementsInterPage.prototype.changeElement = function (elementName) {
        this.dataMeasurements.measurements = [];
        var tempelement = Object.assign({}, (this.typesElement.types.filter(function (element) { return element.name == elementName; })[0]));
        console.log(this.typesElement.types);
        for (var i in tempelement.measurements) {
            this.dataMeasurements.measurements.push({
                side: tempelement.measurements[i],
                quantity: 1,
                value: '',
            });
        }
        //this.dataMeasurements.measurements = element.measurements;
    };
    return MeasurementsInterPage;
}());
MeasurementsInterPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-measurements-inter',
        templateUrl: 'measurements-inter.html',
        providers: [Camera]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        SessionService,
        PTasksMeasurement,
        Platform,
        TasksIssue,
        Camera,
        AlertController,
        CStatus,
        CInstallStatusService,
        CSubMotiveService,
        CTargetService,
        CMotiveService,
        LoadingController,
        TasksInventoryService])
], MeasurementsInterPage);
export { MeasurementsInterPage };
// this.medidasGeneral.push(this.medidas);
//# sourceMappingURL=measurements-inter.js.map