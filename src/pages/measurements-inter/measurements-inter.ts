import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, LoadingController } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { Camera } from '@ionic-native/camera';
import { PTasksMeasurement } from '../../services/tasksmeasurementpost.service';
import { CMotiveService } from '../../services/campaignsmotive.service';
import { CSubMotiveService } from '../../services/campaignssubmotive.service';
import { CTargetService } from '../../services/campaignsissuetarget.service';
import { CInstallStatusService } from '../../services/campaignsinstallstatus.service';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { TasksIssue } from "../../services/tasksissue.service";
import { HomePage } from '../home/home';
import { TasksInventoryService } from '../../services/tasksinventary.service';


 

/**
 * Generated class for the MeasurementsInterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-measurements-inter',
  templateUrl: 'measurements-inter.html',
  providers:[Camera]
})
export class MeasurementsInterPage{

  public image = '';
  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  public sessionS:SessionService,
  			  public TMeasurement:PTasksMeasurement,
          public plt: Platform,
          public taskIs:TasksIssue,
          public camera:Camera,
          public alertCtrl: AlertController,
          public CStatus:CStatus,
          public CInstallStatus:CInstallStatusService,
          public CSubMotive:CSubMotiveService,
          public CTarget:CTargetService,
          public CMotive:CMotiveService,
          public loadingCtrl: LoadingController,
          public TInventoryS:TasksInventoryService

          ) {
  }
  public medidasGeneral= {
    side:'Lateral',
    value:'',
    quantity:null
  }

  public medidasGeneral2= {
    side:'Frontal inferior',
    value:'',
    quantity:null
  }

  public medidasGeneral3= {
    side:'Frontal Superior',
    value:'',
    quantity:null
  }

  public medidasGeneral4= {
    side:'Flejeras',
    value:'',
    quantity:null
  }

  public dataMeasurements ={
    campaign:'',
    code:'',
    store:'',
    file:'', 
    observation:'',
    measurements:[]
  } 
  public dataInstall={
    motive:'',
    submotive: '',
    campaign:'',
    code:'',
    store:'',
    file:'',
    comment:'',
    target:'',
    install_status:''
  }

  public Store;
  public Namecampaign;
  public base64Image: string;
  public nameMedida:string;

  public element = {
    name:'',
  };
  public typesElement={
    types:[],
  };

  implements=[];
  motiveA=[];
  SubmotiveA=[];
  targetA=[];
  statusInstall=[];
  ionViewDidLoad() {
    console.log('ionViewDidLoad MeasurementsInterPage');
    this.nameMedida = this.sessionS.getObject('dataMedida').element;
    this.Namecampaign = this.sessionS.getObject('dataMedida').name;
    this.dataMeasurements.campaign = this.sessionS.getObject('dataMedida')._id;
    this.dataMeasurements.code = this.sessionS.getObject('dataMedida').store.code;
    this.dataMeasurements.store= this.sessionS.getObject('tienda').store._id;
    this.Store = this.sessionS.getObject('tienda').store.name;
    this.dataMeasurements.observation ='';
    this.dataInstall.campaign = this.sessionS.getObject('dataMedida')._id;
    this.dataInstall.code = this.sessionS.getObject('dataMedida').store.code;
    this.dataInstall.store = this.sessionS.getObject('tienda').store._id;
    this.dataInstall.motive='';
    this.dataInstall.submotive='';
    this.dataInstall.comment='';
    this.dataInstall.target='';
    this.dataInstall.install_status='';




    console.log(this.sessionS.getObject('dataMedida').store.code)


    let loader = this.loadingCtrl.create({});
    loader.present();
    this.CMotive.getCMotive()
      .subscribe(response => {
        this.motiveA=response.result;
        console.log(this.motiveA)
      }); 
    this.CSubMotive.getCSubMotive()
      .subscribe(response => {
        this.SubmotiveA=response.result;
        console.log(this.SubmotiveA)
     });
      this.CTarget.getTarget()
      .subscribe(response => {
        this.targetA=response.result;
        console.log(this.targetA)
      }); 
     this.CInstallStatus.getCIStatus()
      .subscribe(response => {
        this.statusInstall=response.result;
        console.log(this.statusInstall);

        this.TMeasurement.getMedidasCode(this.dataMeasurements.code, this.dataMeasurements.store)
        .subscribe(response => {
          console.log(response.result);
          loader.dismiss();
          this.element = {
            name:response.elementType,
          };
          this.typesElement = response.element;
          this.element = Object.assign({}, (this.typesElement.types.filter(element => element.name == this.element.name)[0]));
          this.dataMeasurements.measurements = response.result;
        });

     });         
  }

  foto(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation:true
    }).then((imageData) => {
        this.dataMeasurements.file = imageData;
        this.dataInstall.file=imageData;
        this.image = "data:image/jpeg;base64," + imageData;
        console.log(this.image)        
    }, (err) => {
        console.log(err);
      });
    });
    console.log(this.dataMeasurements.campaign);
  }

  enviarMeasurement(){
    console.log(this.dataMeasurements.measurements);
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.TMeasurement.postTMeasurement(this.dataMeasurements)
      .subscribe(response => {
        loader.dismiss();
        this.navCtrl.setRoot(HomePage);
        console.log(response.result);
      });

  }

  enviarMeasurementIssue(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.taskIs.postTasksIssue(this.dataInstall)
      .subscribe(response => {
        console.log(response.result);
        loader.dismiss();
        this.navCtrl.setRoot(HomePage);
     });
  }

  changeElement(elementName){
    this.dataMeasurements.measurements = [];
    var tempelement = Object.assign({}, (this.typesElement.types.filter(element => element.name == elementName)[0]));
    console.log(this.typesElement.types);
    for (var i in tempelement.measurements) {
      this.dataMeasurements.measurements.push(
       {
         side: tempelement.measurements[i],
         quantity: 1,
         value: '',
       });
    }
    //this.dataMeasurements.measurements = element.measurements;
  }

 }


   // this.medidasGeneral.push(this.medidas);

 