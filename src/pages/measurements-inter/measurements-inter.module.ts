import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MeasurementsInterPage } from './measurements-inter';

@NgModule({
  declarations: [
    MeasurementsInterPage,
  ],
  imports: [
  ],
  exports: [
    MeasurementsInterPage
  ]
})
export class MeasurementsInterPageModule {}
