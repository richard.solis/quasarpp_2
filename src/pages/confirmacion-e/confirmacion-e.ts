import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams , LoadingController, AlertController } from 'ionic-angular';
import { CampaignsCode } from '../../services/bycode.service';
import { SessionService } from "../../services/session.service";
import { PTasksConfirm } from '../../services/tasksconfirmpost.service';
import { HomePage } from '../home/home';
import { Camera } from '@ionic-native/camera';



/**
 * Generated class for the ConfirmacionE page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-confirmacion-e',
  templateUrl: 'confirmacion-e.html',
  providers:[Camera],
})
export class ConfirmacionE {

  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  public campaignscode:CampaignsCode ,
  			  public sessionS:SessionService,
  			  public Pconfirm:PTasksConfirm,
  			  public camera:Camera,
  			  public plt: Platform,
          public alertCtrl: AlertController,
          public loadingCtrl: LoadingController) {

  }
  public DataConfirm={
	  campaign:'',
	  code:'',
	  store:'',
	  file:'',
	  reference:'',
	  comment:''
  }	
 	public campaingsName = '';
 	public namestore='';
 	public image = '';
  ionViewDidLoad() {
    this.namestore= this.sessionS.getObject('tienda').store.name;
    this.DataConfirm.store = this.sessionS.getObject('tienda').store._id;
    console.log(this.namestore);
  }
  buscarCode(id) {
    let loader = this.loadingCtrl.create({});
    loader.present();
      this.campaignscode.getCampaignsCode(id)
      .subscribe(response => {
        this.campaingsName=response.result.name;
        this.DataConfirm.campaign=response.result._id;
        loader.dismiss();
  		}, error => {
          loader.dismiss();
          let alert = this.alertCtrl.create({
            title: 'Error en la Busqueda',
            subTitle: 'Campaña no Existe',
            buttons: ['OK']
          });

          alert.present();
      });
  }

  enviarConfirm(){
    this.Pconfirm.postTConfirm(this.DataConfirm)
      .subscribe(response => {
        console.log(response.result);
      });
     this.navCtrl.setRoot(HomePage);
  }
  foto(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000
    }).then((imageData) => {
        this.DataConfirm.file = imageData;
        this.image = "data:image/jpeg;base64," + imageData;
        console.log(this.image)
 
    }, (err) => {
        console.log(err);
    	});
    });
  }
}

