import { NgModule } from '@angular/core';
import { ConfirmacionE } from './confirmacion-e';

@NgModule({
  declarations: [
    ConfirmacionE,
  ],
  imports: [
  ],
  exports: [
    ConfirmacionE
  ]
})
export class ConfirmacionEModule {}
