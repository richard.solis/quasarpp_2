var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { CampaignsCode } from '../../services/bycode.service';
import { SessionService } from "../../services/session.service";
import { PTasksConfirm } from '../../services/tasksconfirmpost.service';
import { HomePage } from '../home/home';
import { Camera } from '@ionic-native/camera';
/**
 * Generated class for the ConfirmacionE page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ConfirmacionE = (function () {
    function ConfirmacionE(navCtrl, navParams, campaignscode, sessionS, Pconfirm, camera, plt, alertCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.campaignscode = campaignscode;
        this.sessionS = sessionS;
        this.Pconfirm = Pconfirm;
        this.camera = camera;
        this.plt = plt;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.DataConfirm = {
            campaign: '',
            code: '',
            store: '',
            file: '',
            reference: '',
            comment: ''
        };
        this.campaingsName = '';
        this.namestore = '';
        this.image = '';
    }
    ConfirmacionE.prototype.ionViewDidLoad = function () {
        this.namestore = this.sessionS.getObject('tienda').store.name;
        this.DataConfirm.store = this.sessionS.getObject('tienda').store._id;
        console.log(this.namestore);
    };
    ConfirmacionE.prototype.buscarCode = function (id) {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.campaignscode.getCampaignsCode(id)
            .subscribe(function (response) {
            _this.campaingsName = response.result.name;
            _this.DataConfirm.campaign = response.result._id;
            loader.dismiss();
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Error en la Busqueda',
                subTitle: 'Campaña no Existe',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    ConfirmacionE.prototype.enviarConfirm = function () {
        this.Pconfirm.postTConfirm(this.DataConfirm)
            .subscribe(function (response) {
            console.log(response.result);
        });
        this.navCtrl.setRoot(HomePage);
    };
    ConfirmacionE.prototype.foto = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000
            }).then(function (imageData) {
                _this.DataConfirm.file = imageData;
                _this.image = "data:image/jpeg;base64," + imageData;
                console.log(_this.image);
            }, function (err) {
                console.log(err);
            });
        });
    };
    return ConfirmacionE;
}());
ConfirmacionE = __decorate([
    IonicPage(),
    Component({
        selector: 'page-confirmacion-e',
        templateUrl: 'confirmacion-e.html',
        providers: [Camera],
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        CampaignsCode,
        SessionService,
        PTasksConfirm,
        Camera,
        Platform,
        AlertController,
        LoadingController])
], ConfirmacionE);
export { ConfirmacionE };
//# sourceMappingURL=confirmacion-e.js.map