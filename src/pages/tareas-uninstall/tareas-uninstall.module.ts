import { NgModule } from '@angular/core';
import { TareasUninstallPage } from './tareas-uninstall';

@NgModule({
  declarations: [
    TareasUninstallPage,
  ],
  imports: [
  ],
  exports: [
    TareasUninstallPage
  ]
})
export class TareasUninstallPageModule {}
