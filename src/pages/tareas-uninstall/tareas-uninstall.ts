import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { TasksUnistallService } from "../../services/tasksunistall.service"
import { SessionService } from "../../services/session.service";
import { InterInstall } from '../inter-install/inter-install';
import { InterUninstallPage } from '../inter-uninstall/inter-uninstall';
import { TasksPage } from '../tasks/tasks';
import { touchRote } from '../../services/touchrote.service';

/**
 * Generated class for the TareasUninstallPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tareas-uninstall',
  templateUrl: 'tareas-uninstall.html',
})
export class TareasUninstallPage {
 unistall=[];
  constructor(public navCtrl: NavController,
			  public navParams: NavParams,
			  public sessionS:SessionService,
			  public tasksU:TasksUnistallService,
        public loadingCtrl: LoadingController,
        public platform:Platform,
        public touch:touchRote) {
  }

  ionViewDidLoad() {
    this.platform.registerBackButtonAction(() => {
        this.navCtrl.setRoot(TasksPage); 
      });

    let loader = this.loadingCtrl.create({});
    loader.present();
    this.sessionS.setItem('type_task', 'uninstall');
    this.tasksU.getTasksUnistall(this.sessionS.getObject('tienda').store._id)
      .subscribe(response => {
        this.unistall=response.result;
        loader.dismiss();
      }); 
  }

  setUninstall(item){
    console.log(item);
    this.sessionS.setObject('install', item);
    this.navCtrl.push(InterUninstallPage);
    this.touch.postTouch({path: 'Home/' + this.sessionS.getObject('tienda').store.name + '/' +  this.sessionS.getItem('items_task') + '/' + item.name })
        .subscribe( response => {
    });
  }

}
