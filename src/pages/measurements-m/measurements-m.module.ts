import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MeasurementsMPage } from './measurements-m';

@NgModule({
  declarations: [
    MeasurementsMPage,
  ],
  imports: [
  ],
  exports: [
    MeasurementsMPage
  ]
})
export class MeasurementsMPageModule {}
