import { NgModule } from '@angular/core';
import { Contacto } from './contacto';


@NgModule({
  declarations: [
    Contacto,
  ],
  imports: [
  ],
  exports: [
    Contacto
  ]
})
export class ContactoModule {}
