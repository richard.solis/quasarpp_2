import { Component, ViewChild } from '@angular/core';
import {Platform, ViewController, MenuController,} from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Incidencia } from '../incidencia/incidencia';
import { HomePage } from '../home/home';
import { SessionService } from '../../services/session.service';
import { Geolocation } from '@ionic-native/geolocation';
import { MapsService } from '../../services/maps.service';
import { GoogleMap, GoogleMaps, LatLng , GoogleMapsEvent, MarkerOptions, Marker, CameraPosition } from '@ionic-native/google-maps';


/** 
 * Generated class for the Contacto page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contacto',
  templateUrl: 'contacto.html',
  providers:[GoogleMaps, Geolocation]

})
export class Contacto {

   @ViewChild('map') mapElement;

   map: any;

   public mapas=[];
  constructor(public googleMaps: GoogleMaps, 
              public geolocation: Geolocation,
              public viewCtrl: ViewController, 
              public navCtrl: NavController, 
              public navParams: NavParams,
              public plt: Platform,
              public sesionS:SessionService,
              public menuCtrl: MenuController,
              public maps:MapsService) {}


  salir(){
    this.viewCtrl.dismiss();
  }
  
  ionViewDidLoad() {
    this.geolocation.getCurrentPosition().then(pos => {
        console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
        this.maps.getMaps(pos.coords.longitude, pos.coords.latitude)
            .subscribe(response => {
              this.mapas= response.result;
             console.log(response.result)
          });
      });

    

   this.loadMap();
  }























































  
/*
  funciona(){
        let element: HTMLElement = document.getElementById('map');
        let map: GoogleMap = this.googleMaps.create(element);
        let ionic: LatLng = new LatLng(43.0741904, -89.3809802);
        let markeroptions: MarkerOptions = {
          position: ionic,
          title: "hola",
        };
        let marker = map.addMarker(markeroptions).then((marker:Marker) => {
          marker.showInfoWindow();
        })
        let position: CameraPosition = {
           target: ionic,
           zoom: 18,
           tilt: 30
         };
          map.moveCamera(position);
  }*/

  loadMap(){/*
    let element: HTMLElement = document.getElementById('map');
    let map: GoogleMap = this.googleMaps.create(element);

    map.one(GoogleMapsEvent.MAP_READY).then(() => {
       console.log('Map is ready!');
      
     });

    let ionic: LatLng = new LatLng(43.0741904,-89.3809802);

    let position: CameraPosition = {
       target: ionic,
       zoom: 18,
       tilt: 30
     };

     map.moveCamera(position);

     let markerOptions: MarkerOptions = {
       position: ionic,
       title: 'Ionic'
     };

     let marker = map.addMarker(markerOptions).then((marker:Marker) => {
          marker.showInfoWindow();
       });*/
     

  }

  dataR(){
     this.viewCtrl.dismiss();
     this.menuCtrl.open();
  }


}


