var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Platform, ViewController, MenuController, } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionService } from '../../services/session.service';
import { Geolocation } from '@ionic-native/geolocation';
import { MapsService } from '../../services/maps.service';
import { GoogleMaps } from '@ionic-native/google-maps';
/**
 * Generated class for the Contacto page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Contacto = (function () {
    function Contacto(googleMaps, geolocation, viewCtrl, navCtrl, navParams, plt, sesionS, menuCtrl, maps) {
        this.googleMaps = googleMaps;
        this.geolocation = geolocation;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.plt = plt;
        this.sesionS = sesionS;
        this.menuCtrl = menuCtrl;
        this.maps = maps;
        this.mapas = [];
    }
    Contacto.prototype.salir = function () {
        this.viewCtrl.dismiss();
    };
    Contacto.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (pos) {
            console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
            _this.maps.getMaps(pos.coords.longitude, pos.coords.latitude)
                .subscribe(function (response) {
                _this.mapas = response.result;
                console.log(response.result);
            });
        });
        this.loadMap();
    };
    /*
      funciona(){
            let element: HTMLElement = document.getElementById('map');
            let map: GoogleMap = this.googleMaps.create(element);
            let ionic: LatLng = new LatLng(43.0741904, -89.3809802);
            let markeroptions: MarkerOptions = {
              position: ionic,
              title: "hola",
            };
            let marker = map.addMarker(markeroptions).then((marker:Marker) => {
              marker.showInfoWindow();
            })
            let position: CameraPosition = {
               target: ionic,
               zoom: 18,
               tilt: 30
             };
              map.moveCamera(position);
      }*/
    Contacto.prototype.loadMap = function () {
    };
    Contacto.prototype.dataR = function () {
        this.viewCtrl.dismiss();
        this.menuCtrl.open();
    };
    return Contacto;
}());
__decorate([
    ViewChild('map'),
    __metadata("design:type", Object)
], Contacto.prototype, "mapElement", void 0);
Contacto = __decorate([
    IonicPage(),
    Component({
        selector: 'page-contacto',
        templateUrl: 'contacto.html',
        providers: [GoogleMaps, Geolocation]
    }),
    __metadata("design:paramtypes", [GoogleMaps,
        Geolocation,
        ViewController,
        NavController,
        NavParams,
        Platform,
        SessionService,
        MenuController,
        MapsService])
], Contacto);
export { Contacto };
//# sourceMappingURL=contacto.js.map