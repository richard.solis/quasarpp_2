import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController , Platform } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInstallService } from "../../services/tasksinstall.service";
import { InterInstall } from '../inter-install/inter-install';
import { TasksPage } from '../tasks/tasks';
import { touchRote } from '../../services/touchrote.service';
/**
 * Generated class for the TareasPenPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tareas-pen',
  templateUrl: 'tareas-pen.html',
})
export class TareasPenPage {
install=[]
  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  public sessionS:SessionService,
  			  public tasksI:TasksInstallService,
          public loadingCtrl: LoadingController,
          public platform:Platform,
          public touch: touchRote) {
  }

  ionViewDidLoad() {

    this.platform.registerBackButtonAction(() => {
        this.navCtrl.setRoot(TasksPage); 
      });

    let loader = this.loadingCtrl.create({});
    loader.present();
    this.sessionS.setItem('type_task', 'install');
    this.tasksI.getTasksi(this.sessionS.getObject('tienda').store._id)
      .subscribe(response => {
        this.install=response.result; 
      	console.log(this.install);
        loader.dismiss();
      });
  }

  setInstall(item){
    console.log(item);
    this.sessionS.setObject('install', item);
    this.navCtrl.push(InterInstall);
    this.touch.postTouch({path: 'Home/' + this.sessionS.getObject('tienda').store.name + '/' +  this.sessionS.getItem('items_task') + '/' + item.name })
        .subscribe( response => {
    });
    
  }

}
 