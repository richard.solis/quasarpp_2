var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInstallService } from "../../services/tasksinstall.service";
import { InterInstall } from '../inter-install/inter-install';
import { TasksPage } from '../tasks/tasks';
/**
 * Generated class for the TareasPenPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TareasPenPage = (function () {
    function TareasPenPage(navCtrl, navParams, sessionS, tasksI, loadingCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sessionS = sessionS;
        this.tasksI = tasksI;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.install = [];
    }
    TareasPenPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(TasksPage);
        });
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.sessionS.setItem('type_task', 'install');
        this.tasksI.getTasksi(this.sessionS.getObject('tienda').store._id)
            .subscribe(function (response) {
            _this.install = response.result;
            console.log(_this.install);
            loader.dismiss();
        });
    };
    TareasPenPage.prototype.setInstall = function (item) {
        console.log(item);
        this.sessionS.setObject('install', item);
        this.navCtrl.push(InterInstall);
    };
    return TareasPenPage;
}());
TareasPenPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-tareas-pen',
        templateUrl: 'tareas-pen.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        SessionService,
        TasksInstallService,
        LoadingController,
        Platform])
], TareasPenPage);
export { TareasPenPage };
//# sourceMappingURL=tareas-pen.js.map