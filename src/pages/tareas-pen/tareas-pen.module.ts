import { NgModule } from '@angular/core';
import { TareasPenPage } from './tareas-pen';

@NgModule({
  declarations: [
    TareasPenPage,
  ],
  imports: [
  ],
  exports: [
    TareasPenPage
  ]
})
export class TareasPenPageModule {}
