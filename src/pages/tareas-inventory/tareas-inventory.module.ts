import { NgModule } from '@angular/core';
import { TareasInventoryPage } from './tareas-inventory';

@NgModule({
  declarations: [
    TareasInventoryPage,
  ],
  imports: [
  ],
  exports: [
    TareasInventoryPage
  ]
})
export class TareasInventoryPageModule {}
