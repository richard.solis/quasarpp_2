var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInventoryService } from "../../services/tasksinventary.service";
import { InterInventario } from '../inter-inventario/inter-inventario';
import { TasksPage } from '../tasks/tasks';
/**
 * Generated class for the TareasInventoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TareasInventoryPage = (function () {
    function TareasInventoryPage(navCtrl, navParams, sessionS, tasksInventory, loadingCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sessionS = sessionS;
        this.tasksInventory = tasksInventory;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.inventory = [];
        this.userFilter = { name: '' };
        this.temp = [];
    }
    TareasInventoryPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(TasksPage);
        });
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.sessionS.setItem('type_task', 'inventory');
        this.tasksInventory.getTasksInventary(this.sessionS.getObject('tienda').store._id)
            .subscribe(function (response) {
            _this.inventory = response.result;
            _this.temp = response.result;
            console.log("inventory inventory");
            console.log(_this.inventory);
            loader.dismiss();
        });
    };
    TareasInventoryPage.prototype.getItemsName = function (data) {
        var q = data.srcElement.value;
        console.log(q);
        if (q == '') {
            this.inventory = this.temp;
            return;
        }
        this.inventory = this.temp.filter(function (v) {
            if (v.code && q) {
                if (v.code.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
        });
        console.log(q, this.inventory.length);
    };
    TareasInventoryPage.prototype.setInventory = function (item) {
        console.log(item);
        this.sessionS.setObject('install', item);
        this.navCtrl.push(InterInventario);
    };
    return TareasInventoryPage;
}());
TareasInventoryPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-tareas-inventory',
        templateUrl: 'tareas-inventory.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        SessionService,
        TasksInventoryService,
        LoadingController,
        Platform])
], TareasInventoryPage);
export { TareasInventoryPage };
//# sourceMappingURL=tareas-inventory.js.map