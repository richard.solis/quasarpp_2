import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInventoryService } from "../../services/tasksinventary.service";
import { InterInstall } from '../inter-install/inter-install';
import { InterInventario } from '../inter-inventario/inter-inventario';
import { HomePage } from '../home/home';
import { TasksPage } from '../tasks/tasks';
import { touchRote } from '../../services/touchrote.service';



/**
 * Generated class for the TareasInventoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tareas-inventory',
  templateUrl: 'tareas-inventory.html',
})
export class TareasInventoryPage {

	public inventory=[];
  public userFilter: any = { name: '' };
  public temp = [];

  constructor(public navCtrl: NavController,
  			  public navParams: NavParams,
  			  public sessionS:SessionService,
  			  public tasksInventory:TasksInventoryService,
          public loadingCtrl: LoadingController,
          public platform:Platform,
          public touch: touchRote) {
  } 


  ionViewDidLoad() {
    this.platform.registerBackButtonAction(() => {
        this.navCtrl.setRoot(TasksPage); 
      });
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.sessionS.setItem('type_task', 'inventory');
    this.tasksInventory.getTasksInventary(this.sessionS.getObject('tienda').store._id)
      .subscribe(response => {
      	this.inventory = response.result;
        this.temp = response.result;
        console.log("inventory inventory");
        console.log(this.inventory);
        loader.dismiss(); 
      }); 

    

  }


  getItemsName(data) {
    let q = data.srcElement.value;
    console.log(q);

    if (q == '') {
      this.inventory = this.temp;
      return;
    }

    this.inventory = this.temp.filter((v) => {
      if (v.code && q) {
        if (v.code.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });

    console.log(q, this.inventory.length);

  }

  setInventory(item){
    console.log(item);
    this.sessionS.setObject('install', item);
    this.navCtrl.push(InterInventario);
    this.touch.postTouch({path: 'Home/' + this.sessionS.getObject('tienda').store.name + '/' +  this.sessionS.getItem('items_task') + '/' + item.name })
        .subscribe( response => {
    }); 
  }

}
