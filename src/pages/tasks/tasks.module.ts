import { NgModule } from '@angular/core';
import { TasksPage } from './tasks';

@NgModule({
  declarations: [
    TasksPage,
  ],
  imports: [
    
  ],
  exports: [
    TasksPage
  ]
})
export class TasksPageModule {}
