var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, MenuController, Platform } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksStoreService } from "../../services/tasksstore.service";
import { ViewController } from 'ionic-angular';
import { TareasPenPage } from '../tareas-pen/tareas-pen';
import { TareasUninstallPage } from '../tareas-uninstall/tareas-uninstall';
import { TareasInventoryPage } from '../tareas-inventory/tareas-inventory';
import { TareasConfirmPage } from '../tareas-confirm/tareas-confirm';
import { TareasMeasurementsPage } from '../tareas-measurements/tareas-measurements';
import { ContactsPage } from '../contacts/contacts';
import { HomePage } from '../home/home';
/**
 * Generated class for the TasksPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TasksPage = (function () {
    function TasksPage(navCtrl, navParams, tasksS, sessionS, viewCtrl, loadingCtrl, menu, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tasksS = tasksS;
        this.sessionS = sessionS;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.menu = menu;
        this.platform = platform;
        this.global = [];
    }
    TasksPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(HomePage);
        });
        this.menu.swipeEnable(true);
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.tasksS.getTasks(this.sessionS.getObject('tienda').store._id)
            .subscribe(function (response) {
            _this.global = response.result;
            console.log(_this.global);
            for (var _task in _this.global) {
                console.log(_task);
                if (_task == 'install') {
                    _this.global[_task].name = 'Instalación';
                }
                else if (_task == 'uninstall') {
                    _this.global[_task].name = 'Desinstalación';
                }
                else if (_task == 'confirm') {
                    _this.global[_task].name = 'Confirmación';
                }
                else if (_task == 'measurements') {
                    _this.global[_task].name = 'Mediciones';
                }
                else if (_task == 'inventory') {
                    _this.global[_task].name = 'Inventario';
                }
                else if (_task == 'contacts') {
                    _this.global[_task].name = 'Contactos';
                }
            }
            loader.dismiss();
        });
    };
    TasksPage.prototype.install = function (item) {
        console.log(item);
        if (item == 'install') {
            this.navCtrl.push(TareasPenPage);
        }
        else if (item == 'uninstall') {
            this.navCtrl.push(TareasUninstallPage);
        }
        else if (item == 'confirm') {
            this.navCtrl.push(TareasConfirmPage);
        }
        else if (item == 'measurements') {
            this.navCtrl.push(TareasMeasurementsPage);
        }
        else if (item == 'inventory') {
            this.navCtrl.push(TareasInventoryPage);
        }
        else if (item == 'contacts') {
            this.navCtrl.push(ContactsPage);
        }
    };
    return TasksPage;
}());
TasksPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-tasks',
        templateUrl: 'tasks.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        TasksStoreService,
        SessionService,
        ViewController,
        LoadingController,
        MenuController,
        Platform])
], TasksPage);
export { TasksPage };
//# sourceMappingURL=tasks.js.map