import { Component, } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,  MenuController, Platform  } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksStoreService } from "../../services/tasksstore.service";
import { ViewController } from 'ionic-angular';
import { TareasPenPage } from '../tareas-pen/tareas-pen';
import { TareasUninstallPage } from '../tareas-uninstall/tareas-uninstall';
import { TareasInventoryPage } from '../tareas-inventory/tareas-inventory';
import { TareasConfirmPage } from '../tareas-confirm/tareas-confirm';
import { TareasMeasurementsPage } from '../tareas-measurements/tareas-measurements';
import { ContactsPage } from '../contacts/contacts';
import { HomePage } from '../home/home';
import { touchRote } from '../../services/touchrote.service';
/**
 * Generated class for the TasksPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tasks',
  templateUrl: 'tasks.html',
})
export class TasksPage {
  global=[]
 

  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams, 
  			  public tasksS:TasksStoreService, 
  			  public sessionS:SessionService,
  			  public viewCtrl: ViewController,
          public loadingCtrl: LoadingController,
          public menu:MenuController,
          public platform:Platform,
          public touch: touchRote   ) {
  } 

  ionViewDidLoad() {
       
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.setRoot(HomePage); 
      });
 
    
     
    this.menu.swipeEnable(true);
    let loader = this.loadingCtrl.create({});
    loader.present();
 	this.tasksS.getTasks(this.sessionS.getObject('tienda').store._id)
      .subscribe(response => {
      	this.global=response.result;
      	console.log(this.global);
        for (var _task in this.global) {
          console.log(_task);
          if(_task == 'install'){ 
            this.global[_task].name = 'Instalación';
          }
          else if(_task == 'uninstall'){
            this.global[_task].name = 'Desinstalación';
          }
          else if(_task == 'confirm'){
            this.global[_task].name = 'Confirmación';
          }
          else if(_task == 'measurements'){
            this.global[_task].name = 'Mediciones';
          }
          else if(_task == 'inventory'){
            this.global[_task].name = 'Inventario';
          }
          else if(_task == 'contacts'){
            this.global[_task].name = 'Contactos';
          }
        }
        loader.dismiss();
      });   
   }
   install(item){
     console.log(item);
     if(item == 'install'){
       this.navCtrl.push(TareasPenPage);
     }
     else if(item == 'uninstall'){
       this.navCtrl.push(TareasUninstallPage);
     }
     else if(item == 'confirm'){
       this.navCtrl.push(TareasConfirmPage);
     }
     else if(item == 'measurements'){
       this.navCtrl.push(TareasMeasurementsPage);
     }
     else if(item == 'inventory'){
       this.navCtrl.push(TareasInventoryPage);
     }
     else if(item == 'contacts'){
       this.navCtrl.push(ContactsPage);
     }
      this.sessionS.setItem('items_task', item);
     this.touch.postTouch({path: 'Home/' + this.sessionS.getObject('tienda').store.name + '/' + item})
        .subscribe( response => {
    });

  }
}
