import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ContactsInterPage } from './contacts-inter';

@NgModule({
  declarations: [
    ContactsInterPage,
  ],
  imports: [
  ],
  exports: [
    ContactsInterPage
  ]
})
export class ContactsInterPageModule {}
