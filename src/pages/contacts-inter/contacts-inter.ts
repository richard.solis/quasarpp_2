import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , LoadingController, AlertController} from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { ContactsPage } from '../contacts/contacts';
import { StoreContact } from '../../services/storecontact.service';
import { HomePage } from '../home/home';
import { TContact } from '../../services/taskscontact.service';



/**
 * Generated class for the ContactsInterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-contacts-inter',
  templateUrl: 'contacts-inter.html',
})
export class ContactsInterPage {

  public dataContact ={
    store:'',
    position:'',
    firstname:'',
    lastname:'',
    phone:'',
    email:''
  } 




  constructor(public navCtrl: NavController,
  			  public navParams: NavParams,
  			  public sessionS: SessionService,
          public StoreC:StoreContact,
          public loadingCtrl: LoadingController,
          public alertCtrl:AlertController,
          public contact:TContact) {
  }
  public nameContact;
  public Store;
  public contacto;

  ionViewDidLoad() {
    this.nameContact = this.sessionS.getItem('contactName');
    this.Store = this.sessionS.getObject('tienda').store.name;
    this.dataContact.store= this.sessionS.getObject('tienda').store._id;
    this.dataContact.position=this.sessionS.getItem('contactName');
    this.dataContact.firstname='';
    this.dataContact.lastname='';
    this.dataContact.phone='';
    this.dataContact.email='';
    console.log(this.sessionS.getObject('tienda').store);
    this.contacto = this.sessionS.getObject('tienda').store.contacts.filter(contact => contact.position == this.dataContact.position)[0];
    if(this.contacto){
      this.dataContact =this.contacto;
    }
  } 
  enviarContact(){
    let loader = this.loadingCtrl.create({  });
      loader.present();

      console.log(this.dataContact);
    if(this.contacto){
      this.contact.PutContact(this.sessionS.getObject('tienda').store._id,this.contacto)
        .subscribe(response => {
          loader.dismiss();
          this.navCtrl.setRoot(HomePage);;
          let alert = this.alertCtrl.create({
            title: 'Mensaje',
            subTitle: 'Su Contacto a sido modificado',
            buttons:['ok']
          });
        alert.present();
        })
    }
    else{
      this.StoreC.postSContact(this.dataContact)
      .subscribe(response => {
        console.log(response.result);
        loader.dismiss();

        this.navCtrl.setRoot(HomePage);;
        let alert = this.alertCtrl.create({
          title: 'Mensaje',
          subTitle: 'Su Contacto a sido enviada',
          buttons:['ok']
        });
      alert.present();
     });
    }
      
  }

}
