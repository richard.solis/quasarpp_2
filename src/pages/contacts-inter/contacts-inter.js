var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { StoreContact } from '../../services/storecontact.service';
import { HomePage } from '../home/home';
import { TContact } from '../../services/taskscontact.service';
/**
 * Generated class for the ContactsInterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ContactsInterPage = (function () {
    function ContactsInterPage(navCtrl, navParams, sessionS, StoreC, loadingCtrl, alertCtrl, contact) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sessionS = sessionS;
        this.StoreC = StoreC;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.contact = contact;
        this.dataContact = {
            store: '',
            position: '',
            firstname: '',
            lastname: '',
            phone: '',
            email: ''
        };
    }
    ContactsInterPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.nameContact = this.sessionS.getItem('contactName');
        this.Store = this.sessionS.getObject('tienda').store.name;
        this.dataContact.store = this.sessionS.getObject('tienda').store._id;
        this.dataContact.position = this.sessionS.getItem('contactName');
        this.dataContact.firstname = '';
        this.dataContact.lastname = '';
        this.dataContact.phone = '';
        this.dataContact.email = '';
        console.log(this.sessionS.getObject('tienda').store);
        this.contacto = this.sessionS.getObject('tienda').store.contacts.filter(function (contact) { return contact.position == _this.dataContact.position; })[0];
        if (this.contacto) {
            this.dataContact = this.contacto;
        }
    };
    ContactsInterPage.prototype.enviarContact = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        console.log(this.dataContact);
        if (this.contacto) {
            this.contact.PutContact(this.sessionS.getObject('tienda').store._id, this.contacto)
                .subscribe(function (response) {
                loader.dismiss();
                _this.navCtrl.setRoot(HomePage);
                ;
                var alert = _this.alertCtrl.create({
                    title: 'Mensaje',
                    subTitle: 'Su Contacto a sido modificado',
                    buttons: ['ok']
                });
                alert.present();
            });
        }
        else {
            this.StoreC.postSContact(this.dataContact)
                .subscribe(function (response) {
                console.log(response.result);
                loader.dismiss();
                _this.navCtrl.setRoot(HomePage);
                ;
                var alert = _this.alertCtrl.create({
                    title: 'Mensaje',
                    subTitle: 'Su Contacto a sido enviada',
                    buttons: ['ok']
                });
                alert.present();
            });
        }
    };
    return ContactsInterPage;
}());
ContactsInterPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-contacts-inter',
        templateUrl: 'contacts-inter.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        SessionService,
        StoreContact,
        LoadingController,
        AlertController,
        TContact])
], ContactsInterPage);
export { ContactsInterPage };
//# sourceMappingURL=contacts-inter.js.map