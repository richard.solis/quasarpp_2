var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TContact } from "../../services/taskscontact.service";
import { ContactsInterPage } from '../contacts-inter/contacts-inter';
import { TasksPage } from '../tasks/tasks';
import { HomePage } from '../home/home';
/**
 * Generated class for the ContactsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ContactsPage = (function () {
    function ContactsPage(navCtrl, navParams, sessionS, contact, loadingCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sessionS = sessionS;
        this.contact = contact;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.Contactos = [];
        this.type_task = '';
    }
    ContactsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(TasksPage);
        });
        this.type_task = this.navParams.get('task');
        var loader = this.loadingCtrl.create({});
        loader.present();
        if (this.type_task == 'Contactos') {
            this.platform.registerBackButtonAction(function () {
                _this.navCtrl.setRoot(HomePage);
            });
            this.contact.getTContactPrueba()
                .subscribe(function (response) {
                _this.Contactos = response.result;
                console.log(_this.Contactos);
                loader.dismiss();
            });
        }
        else {
            this.contact.getTContact(this.sessionS.getObject('tienda').store._id)
                .subscribe(function (response) {
                _this.Contactos = response.result;
                console.log(_this.Contactos);
                loader.dismiss();
            });
        }
        ;
    };
    ContactsPage.prototype.contactButton = function (item) {
        console.log(item);
        this.sessionS.setItem('contactName', item);
        this.navCtrl.push(ContactsInterPage);
    };
    return ContactsPage;
}());
ContactsPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-contacts',
        templateUrl: 'contacts.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        SessionService,
        TContact,
        LoadingController,
        Platform])
], ContactsPage);
export { ContactsPage };
//# sourceMappingURL=contacts.js.map