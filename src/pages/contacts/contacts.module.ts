import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ContactsPage } from './contacts';

@NgModule({
  declarations: [
    ContactsPage,
  ],
  imports: [
 
  ],
  exports: [
    ContactsPage
  ]
})
export class ContactsPageModule {}
