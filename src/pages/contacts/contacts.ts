import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform  } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TContact } from "../../services/taskscontact.service";
import { ContactsInterPage } from '../contacts-inter/contacts-inter';
import { TasksPage } from '../tasks/tasks';
import { HomePage } from '../home/home';
import { touchRote } from '../../services/touchrote.service';

/**
 * Generated class for the ContactsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {

  constructor(public navCtrl: NavController,
  			  public navParams: NavParams,
  			  public sessionS:SessionService,
  			  public contact:TContact,
          public loadingCtrl: LoadingController,
          public platform:Platform,
          public touch: touchRote ) {
  }

  public Contactos = [];
  public phones = [];
  public type_task = '';

  ionViewDidLoad() {
    this.platform.registerBackButtonAction(() => {
        this.navCtrl.setRoot(TasksPage); 
      });
    this.type_task = this.navParams.get('task');
    let loader = this.loadingCtrl.create({});
    loader.present();

    if (this.type_task == 'Contactos' ){
      this.platform.registerBackButtonAction(() => {
        this.navCtrl.setRoot(HomePage); 
      });
      this.contact.getTContactPrueba()
      .subscribe(response => {
        this.Contactos=response.result;
          console.log(this.Contactos);
          this.contact.getTContactNumber(this.sessionS.getObject('tienda').store._id)
          .subscribe(response => {
            var tPhones = response.result;
            for (var i = this.Contactos.length - 1; i >= 0; i--) {
              this.phones[i] = (tPhones.filter(phone => phone.position == this.Contactos[i])[0])?tPhones.filter(phone => phone.position == this.Contactos[i])[0].phone:'';
            }
            console.log(this.phones);
            loader.dismiss();
          });
      })
    }
    else {
    this.contact.getTContact(this.sessionS.getObject('tienda').store._id)
      .subscribe(response => {
      		this.Contactos=response.result;
          console.log(this.Contactos);
          loader.dismiss();
          for (var i = this.Contactos.length - 1; i >= 0; i--) {
            this.phones[i] = '';
          }
      })
    };

    
  }

  contactButton(item){

  	console.log(item);
  	 this.sessionS.setItem('contactName', item);
  	 this.navCtrl.push(ContactsInterPage);
     this.touch.postTouch({path: 'Home/' + this.sessionS.getObject('tienda').store.name + '/' +  this.sessionS.getItem('items_task') + '/' + item })
        .subscribe( response => {
     }); 
  }

}
