import { NgModule } from '@angular/core';
import { TareasConfirmPage } from './tareas-confirm';

@NgModule({
  declarations: [
    TareasConfirmPage,
  ],
  imports: [
  ],
  exports: [
    TareasConfirmPage
  ]
})
export class TareasConfirmPageModule {}
