var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { SessionService } from "../../services/session.service";
import { TasksConfirmService } from "../../services/tasksconfirm.service";
import { TContact } from "../../services/taskscontact.service";
import { ConfirmInterPage } from '../confirm-inter/confirm-inter';
import { TasksPage } from '../tasks/tasks';
/**
 * Generated class for the TareasConfirmPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TareasConfirmPage = (function () {
    function TareasConfirmPage(navCtrl, navParams, sessionS, tasksC, contact, plt, loadingCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sessionS = sessionS;
        this.tasksC = tasksC;
        this.contact = contact;
        this.plt = plt;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.confirm = [];
    }
    TareasConfirmPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            _this.navCtrl.setRoot(TasksPage);
        });
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.sessionS.setItem('type_task', 'confirm');
        this.tasksC.getTasksConfirm(this.sessionS.getObject('tienda').store._id)
            .subscribe(function (response) {
            _this.confirm = response.result;
            console.log(_this.confirm);
            loader.dismiss();
        });
    };
    TareasConfirmPage.prototype.ConfirmInter = function (item) {
        this.sessionS.setObject('confirmaData', item);
        this.navCtrl.push(ConfirmInterPage);
    };
    return TareasConfirmPage;
}());
TareasConfirmPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-tareas-confirm',
        templateUrl: 'tareas-confirm.html',
        providers: [Camera]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        SessionService,
        TasksConfirmService,
        TContact,
        Platform,
        LoadingController,
        Platform])
], TareasConfirmPage);
export { TareasConfirmPage };
//# sourceMappingURL=tareas-confirm.js.map