import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { SessionService } from "../../services/session.service";
import { TasksConfirmService } from "../../services/tasksconfirm.service";
import { TContact } from "../../services/taskscontact.service";
import { PTasksInventory } from '../../services/tasksinventorypost.service';
import { ConfirmInterPage } from '../confirm-inter/confirm-inter';
import { TasksPage } from '../tasks/tasks';
import { touchRote } from '../../services/touchrote.service';

/**
 * Generated class for the TareasConfirmPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation. 
 */
@IonicPage()
@Component({
  selector: 'page-tareas-confirm',
  templateUrl: 'tareas-confirm.html',
  providers:[Camera] 
})
export class TareasConfirmPage {
confirm=[]

  constructor(public navCtrl: NavController,
  			  public navParams: NavParams,
  			  public sessionS:SessionService,
  			  public tasksC:TasksConfirmService,
          public contact:TContact,
          public plt: Platform,
          public loadingCtrl: LoadingController,
          public platform:Platform,
          public touch: touchRote ) {
  }

  ionViewDidLoad() {
    this.platform.registerBackButtonAction(() => {
        this.navCtrl.setRoot(TasksPage); 
      });
    let loader = this.loadingCtrl.create({});
    loader.present();
   this.sessionS.setItem('type_task', 'confirm');
   	this.tasksC.getTasksConfirm(this.sessionS.getObject('tienda').store._id)
      .subscribe(response => {
        this.confirm=response.result;
      	console.log(this.confirm);
          loader.dismiss(); 
      }); 
  }

  ConfirmInter(item){
    this.sessionS.setObject('confirmaData', item);
    this.navCtrl.push(ConfirmInterPage)
    this.touch.postTouch({path: 'Home/' + this.sessionS.getObject('tienda').store.name + '/' +  this.sessionS.getItem('items_task') + '/' + item.name })
        .subscribe( response => {
    }); 
  }
  

}
