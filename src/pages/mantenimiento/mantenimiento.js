var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { MaintenanceS } from '../../services/maintenance.service';
import { SessionService } from "../../services/session.service";
import { Camera } from '@ionic-native/camera';
import { HomePage } from '../home/home';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { CampaignsCode } from '../../services/bycode.service';
/**
 * Generated class for the MantenimientoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MantenimientoPage = (function () {
    function MantenimientoPage(navCtrl, navParams, maintenance, sessionS, camera, plt, alertCtrl, CStatus, loadingCtrl, campaignscode) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.maintenance = maintenance;
        this.sessionS = sessionS;
        this.camera = camera;
        this.plt = plt;
        this.alertCtrl = alertCtrl;
        this.CStatus = CStatus;
        this.loadingCtrl = loadingCtrl;
        this.campaignscode = campaignscode;
        this.maintenace = {
            campaign: '',
            code: '',
            store: '',
            comment: '',
            fileBefore: '',
            fileAfter: ''
        };
        this.nameMaintenace = '';
        this.nameStore = '';
        this.imageBefore = '';
        this.imageAfter = '';
        this.campaings = [];
        this.campaingsName = '';
    }
    MantenimientoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.nameStore = this.sessionS.getObject('tienda').store.name;
        this.maintenace.store = this.sessionS.getObject('tienda').store._id;
        console.log(this.nameStore);
        this.CStatus.getCampaings()
            .subscribe(function (response) {
            _this.campaings = response.result;
            console.log("esteeste");
            console.log(_this.campaings);
        });
    };
    MantenimientoPage.prototype.foto1 = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000,
                correctOrientation: true
            }).then(function (imageData) {
                _this.maintenace.fileBefore = imageData;
                _this.imageBefore = "data:image/jpeg;base64," + imageData;
                console.log(_this.imageBefore);
            }, function (err) {
                console.log(err);
            });
        });
    };
    MantenimientoPage.prototype.foto2 = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000,
                correctOrientation: true
            }).then(function (imageData) {
                _this.maintenace.fileAfter = imageData;
                _this.imageAfter = "data:image/jpeg;base64," + imageData;
                console.log(_this.imageAfter);
            }, function (err) {
                console.log(err);
            });
        });
    };
    MantenimientoPage.prototype.enviarMantenimiento = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.maintenance.postMaintenance(this.maintenace)
            .subscribe(function (response) {
            loader.dismiss();
            _this.navCtrl.setRoot(HomePage);
        });
    };
    MantenimientoPage.prototype.buscarCode = function (id) {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.campaignscode.getCampaignsCode(id)
            .subscribe(function (response) {
            _this.campaingsName = response.result.name;
            _this.maintenace.campaign = response.result._id;
            loader.dismiss();
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Error en la Busqueda',
                subTitle: 'Campaña no Existe',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    return MantenimientoPage;
}());
MantenimientoPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-mantenimiento',
        templateUrl: 'mantenimiento.html',
        providers: [Camera],
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        MaintenanceS,
        SessionService,
        Camera,
        Platform,
        AlertController,
        CStatus,
        LoadingController,
        CampaignsCode])
], MantenimientoPage);
export { MantenimientoPage };
//# sourceMappingURL=mantenimiento.js.map