import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { MaintenanceS } from '../../services/maintenance.service';
import { SessionService } from "../../services/session.service";
import { Camera } from '@ionic-native/camera';
import { HomePage } from '../home/home';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { CampaignsCode } from '../../services/bycode.service';


/**
 * Generated class for the MantenimientoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-mantenimiento',
  templateUrl: 'mantenimiento.html',
  providers:[Camera],
})
export class MantenimientoPage {

  public maintenace={
  	campaign:'',
  	code:'',
  	store:'',
  	comment:'',
  	fileBefore:'',
  	fileAfter:''
  }

  constructor(public navCtrl: NavController,
      			  public navParams: NavParams,
      			  public maintenance:MaintenanceS,
      			  public sessionS:SessionService,
      			  public camera:Camera,
      			  public plt: Platform,
      			  public alertCtrl:AlertController,
      			  public CStatus:CStatus,
              public loadingCtrl: LoadingController,
              public campaignscode:CampaignsCode
         ) {
  }
  public nameMaintenace = '';
  public nameStore = '';
  public imageBefore='';
  public imageAfter='';
  public campaings=[];
  public campaingsName = '';
  ionViewDidLoad() {
  	this.nameStore = this.sessionS.getObject('tienda').store.name;
  	this.maintenace.store = this.sessionS.getObject('tienda').store._id;
    console.log(this.nameStore);

    this.CStatus.getCampaings()
          .subscribe(response => {
            this.campaings=response.result;
            console.log("esteeste");
            console.log(this.campaings);
            });

  }
  
  foto1(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation:true
    }).then((imageData) => {
        this.maintenace.fileBefore = imageData;
        this.imageBefore = "data:image/jpeg;base64," + imageData;
        console.log(this.imageBefore)
 
    }, (err) => {
        console.log(err);
    	});
    });
  }
  foto2(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation:true
    }).then((imageData) => {
        this.maintenace.fileAfter = imageData;
        this.imageAfter = "data:image/jpeg;base64," + imageData;
        console.log(this.imageAfter)
 
    }, (err) => {
        console.log(err);
    	});
    });
  }

  enviarMantenimiento(){

    let loader = this.loadingCtrl.create({});
    loader.present();
    this.maintenance.postMaintenance(this.maintenace)
      .subscribe(response => {
         
          loader.dismiss();
          this.navCtrl.setRoot(HomePage);               
      });
      
  }

  buscarCode(id) {
    let loader = this.loadingCtrl.create({});
    loader.present();
      this.campaignscode.getCampaignsCode(id)
      .subscribe(response => {
        this.campaingsName=response.result.name;
        this.maintenace.campaign=response.result._id;
        

        loader.dismiss();
      }, error => {
          loader.dismiss();
          let alert = this.alertCtrl.create({
            title: 'Error en la Busqueda',
            subTitle: 'Campaña no Existe',
            buttons: ['OK']
          });
          alert.present();
      }); 
      
  }

}
