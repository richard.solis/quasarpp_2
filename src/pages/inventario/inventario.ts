import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InterInventario } from '../inter-inventario/inter-inventario';


/**
 * Generated class for the Inventario page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-inventario',
  templateUrl: 'inventario.html',
  
})
export class Inventario {
  newItem:string="";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Inventario');
  }

  ja(){
    this.navCtrl.push(InterInventario);
  }
 
}
  