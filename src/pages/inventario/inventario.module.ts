import { NgModule } from '@angular/core';
import { Inventario } from './inventario';

@NgModule({
  declarations: [
    Inventario,
  ],
  imports: [
  ],
  exports: [
    Inventario
  ]
})
export class InventarioModule {}
