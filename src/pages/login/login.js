var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { UserService } from "../../services/user.service";
import { FormBuilder, Validators } from "@angular/forms";
import { SessionService } from "../../services/session.service";
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { UserProfile } from '../../broadcasting/util/user-profile';
var loginPage = (function () {
    function loginPage(navCtrl, alertCtrl, loadingCtrl, _fb, userService, session, profile, menu, touch) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this._fb = _fb;
        this.userService = userService;
        this.session = session;
        this.profile = profile;
        this.menu = menu;
        this.touch = touch;
    }
    loginPage.prototype.ionViewDidLoad = function () {
        this.menu.swipeEnable(false);
    };
    loginPage.prototype.ngOnInit = function () {
        if (this.session.getObject('user')) {
            this.navCtrl.setRoot(HomePage);
        }
        this.userForm = this._fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    };
    loginPage.prototype.onSubmit = function (model, isValid) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Porfavor Espere...",
            duration: 1000
        });
        loader.present();
        if (isValid) {
            this.userService.login(model)
                .subscribe(function (response) {
                _this.session.setItem('token', response.token);
                _this.session.setObject('user', response.result);
                _this.profile.fire({ name: response.result.firstname });
                _this.navCtrl.setRoot(HomePage);
                _this.touch.postTouch(_this.navCtrl.setRoot(HomePage))
                    .subscribe(function (response) {
                });
            }, function (error) {
                var alert = _this.alertCtrl.create({
                    title: 'Error al Entrar',
                    subTitle: 'Verifica tu correo y contraseña',
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    return loginPage;
}());
loginPage = __decorate([
    Component({
        selector: 'page-login',
        templateUrl: 'login.html',
        providers: [
            FormBuilder, UserService, SessionService, UserProfile
        ]
    }),
    __metadata("design:paramtypes", [NavController,
        AlertController,
        LoadingController,
        FormBuilder,
        UserService,
        SessionService,
        UserProfile,
        MenuController, Object])
], loginPage);
export { loginPage };
//# sourceMappingURL=login.js.map