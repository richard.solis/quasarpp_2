import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { UserService } from "../../services/user.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserEntity } from "../../services/user.entity";
import { SessionService } from "../../services/session.service";
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { UserProfile } from '../../broadcasting/util/user-profile';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [
    FormBuilder, UserService, SessionService, UserProfile
  ]
})

export class loginPage implements OnInit{
  
  
  public userForm:FormGroup;

constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _fb:FormBuilder,
    private userService:UserService,
    private session:SessionService,
    private profile:UserProfile,
    public menu: MenuController,
    ) {

}
ionViewDidLoad(){
    this.menu.swipeEnable(false);
  }


ngOnInit() {

  if(this.session.getObject('user')){
    this.navCtrl.setRoot(HomePage);
  }

  this.userForm = this._fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
}

onSubmit(model:UserEntity, isValid:Boolean){
        let loader = this.loadingCtrl.create({
        content: "Porfavor Espere...",
        duration: 1000
         });
    loader.present();
 
    if(isValid){
      this.userService.login(model)
        .subscribe(
          response => {
            this.session.setItem('token', response.token);
            this.session.setObject('user', response.result);
             this.profile.fire({name: response.result.firstname});
            this.navCtrl.setRoot(HomePage);
          },
          error => {
              let alert = this.alertCtrl.create({
                title: 'Error al Entrar',
                subTitle: 'Verifica tu correo y contraseña',
                buttons: ['OK']
              });
              alert.present();
            }
        );
    }

  }

}
