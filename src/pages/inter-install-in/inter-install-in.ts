import { Component } from '@angular/core';
import { Platform, AlertController, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInstallService } from "../../services/tasksinstall.service";
import { TasksUnistallService } from "../../services/tasksunistall.service";
import { TasksMeasuService } from "../../services/tasksmeasurements.service";
import { TasksInventoryService } from "../../services/tasksinventary.service";
import { TasksIssue } from "../../services/tasksissue.service";
import { CMotiveService } from '../../services/campaignsmotive.service';
import { CSubMotiveService } from '../../services/campaignssubmotive.service';
import { CTargetService } from '../../services/campaignsissuetarget.service';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { CCAtegoriaService } from '../../services/campaignsissuecategoria.service';
import { CInstallStatusService } from '../../services/campaignsinstallstatus.service';
import { HomePage } from '../home/home';
import { FileSelectDirective, FileDropDirective, FileUploader, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { AppSettings } from '../../app.settings';
import { CampaignsCode } from '../../services/bycode.service';
import { PTasksInventory } from '../../services/tasksinventorypost.service';

/**
 * Generated class for the InterInstallPage page. 
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-inter-install-in',
  templateUrl: 'inter-install-in.html',
  providers:[Camera] 
})
export class InterInstallIn{

  public uploader:FileUploader = new FileUploader({url: AppSettings.BASE_PATH + AppSettings});

 public dataInventory = {
    campaign:'',
    code:'',
    store:'',
    file:'',
    implementer:'Quasar',
    comment:'',
  }

	public dataSend = {
		campaign:'',
		code:'',
		store:'',
		file:'',
    target:'',
    comment:'',
    motive:'',
    submotive: '',
    install_status: ''
	}

  public dataSendInstall = {
    campaign:'',
    code:'',
    store:'',
    comment:'',
    file:'',
    target:''
  }

  public dataInvetarioOtro = {
    target: 'Inventario',
    store:this.sessionS.getObject('tienda').store._id,
    file: '',
    motive: 'Elemento no instalado por Quasar',
    submotive: 'Instalado por proveedor no autorizado',
    element: '',
    quantity: null,
    material: '',
    client: '',
    brand: '',
    responsable: '',
    signed: false,
    comment: '',
    category: ''
  }

	public image = '';
	public type_task = '';
  public campaingsName = '';
  public location = [];
  constructor(public sessionS:SessionService, 
              public tasksI:TasksInstallService, 
              public tasksU:TasksUnistallService, 
              public tasksM:TasksMeasuService, 
              public tasksIn:TasksInventoryService, 
              public navCtrl: NavController, 
              public navParams: NavParams, 
              public plt: Platform, 
              public camera:Camera,
              public taskIs:TasksIssue,
              public CMotive:CMotiveService,
              public CSubMotive:CSubMotiveService,
              public CTarget:CTargetService,
              public CStatus:CStatus,
              public CScategoria:CCAtegoriaService,
              public CInstallStatus:CInstallStatusService,
              public alertCtrl:AlertController,
              public loadingCtrl: LoadingController,
              public campaignscode:CampaignsCode,
             public PTInventory:PTasksInventory,){
  }
  public base64Image: string;
  motiveA=[];
  campaings=[];
  campaings2=[];
  SubmotiveA=[];
  targetA=[];
  statusA=[];
  categoriaA=[];
  statusInstall=[];
  Materiales=[];
  Cliente=[];
  Element=[];
  CategoriI=[];
  namestore = [];
  public userFilter: any = { name: '' };
  ionViewDidLoad() {

    this.type_task = this.navParams.get('task');
    console.log('ionViewDidLoad InterInstall hh');
    this.namestore= this.sessionS.getObject('tienda').store.name;
    this.dataSend.campaign = "";
    this.dataSendInstall.campaign = "";
    this.dataSend.code = "";
    this.dataSendInstall.code = "";
    this.dataSend.store= this.sessionS.getObject('tienda').store._id;
    this.dataInventory.store = this.sessionS.getObject('tienda').store._id;
    this.dataSendInstall.store= this.sessionS.getObject('tienda').store._id;
    this.dataSend.target= this.type_task;
    this.dataSendInstall.target = this.type_task;
    this.dataSend.motive= '';
    this.dataSend.comment= '';
    this.dataSendInstall.comment= '';
    this.dataSend.submotive= '';
    this.dataSend.install_status= '';

    this.CInstallStatus.getCIStatus()
      .subscribe(response => {
        if(this.type_task == 'Instalación'){
          this.statusInstall=response.result;
        }
        else if(this.type_task == 'Desinstalación' || this.type_task == 'Inventario'){
          this.statusInstall = ['REALIZADO', 'NO REALIZADO'];
        }
        console.log(this.statusInstall)
        this.CStatus.getCampaings()
          .subscribe(response => {
            this.campaings=response.result;
            this.campaings2=response.result;
            console.log("esteeste");
            console.log(this.campaings);
            this.CStatus.getIStatus()
              .subscribe(response => {
                this.statusA=response.result;    
                this.CMotive.getCMotive()
                  .subscribe(response => {
                    this.motiveA=response.result;
                    console.log(this.motiveA)
                    this.CSubMotive.getCSubMotive()
                      .subscribe(response => {
                        this.SubmotiveA=response.result;
                        console.log(this.SubmotiveA)
                     });
                  }); 
              }); 
          }); 
      });
      this.tasksIn.getMaterialsInventary()
      .subscribe(response => {
        this.Materiales = response.result;
        console.log(this.Materiales)
      });

      this.tasksIn.getClienteInventary()
      .subscribe(response => {
        this.Cliente = response.result;
        console.log(this.Cliente)
      });

      this.tasksIn.getElementInventary()
      .subscribe(response => {
        this.Element = response.result;
        console.log(this.Element)
      });

      this.tasksIn.getCategoriInventary()
      .subscribe(response => {
        for (var i = 0; i < response.result.length; i++) {
          for (var k = 0; k < response.result[i].categories.length; k++) {
            this.CategoriI.push(response.result[i].categories[k].name);
          }
        }
      });
  }

  funcionInstalado(){ 
    this.type_task = this.sessionS.getItem('type_task');
      this.tasksI.setInstall(this.dataSendInstall)
        .subscribe(response => {
          console.log(response.result)
          this.dataSendInstall.comment=this.dataSend.comment;
       }); 

    let a=this.navCtrl.setRoot(HomePage);
    let alert = this.alertCtrl.create({
        title: 'Mensaje',
        subTitle: 'Instalacion Exitosa',
        buttons:['ok']
      });
    alert.present();

 
  }

  foto(){
    this.plt.ready().then((readySource) => {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        correctOrientation:true

    }).then((imageData) => {
        this.dataSend.file = imageData;
        this.dataSendInstall.file = imageData;
        this.dataInvetarioOtro.file = imageData;
        this.dataInventory.file = imageData;
        this.image = "data:image/jpeg;base64," + imageData;
        console.log(this.image)
 
    }, (err) => {
        console.log(err);
    	});
    });
  }



  enviarInstalacion(){
    this.taskIs.postTasksIssue(this.dataSend)
      .subscribe(response => {
        console.log(response.result);
      });
      let a=this.navCtrl.setRoot(HomePage);
      let alert = this.alertCtrl.create({
        title: 'Mensaje',
        subTitle: 'Su Incidencia a sido enviada',
        buttons:['ok']
      });
    alert.present();
      
  }

  buscarCode(id, code) {
    let loader = this.loadingCtrl.create({});
    loader.present();
      this.campaignscode.getCampaignsCode(id)
      .subscribe(response => {
        this.campaingsName=response.result.name;
        this.dataSendInstall.campaign=response.result._id;
        this.dataInventory.campaign=response.result._id;
        this.dataSend.campaign=this.campaingsName;
        this.dataSendInstall.code = this.dataSend.code;
        this.dataInventory.code =  this.dataSend.code;

        console.log(response.result.elements);
        this.location = response.result.elements.filter( item => item.code == this.dataSend.code.split('-')[0])[0].location;
        loader.dismiss();
        console.log(this.location);
      }, error => {
          loader.dismiss();
          let alert = this.alertCtrl.create({
            title: 'Error en la Busqueda',
            subTitle: 'Campaña no Existe',
            buttons: ['OK']
          });
          alert.present();
      }); 
      
  }

  funcionInventarioOtro(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    if(!this.dataInvetarioOtro.brand || !this.dataInvetarioOtro.client || !this.dataInvetarioOtro.element){
      loader.dismiss();
      return;
    }
    this.taskIs.postTasksIssue(this.dataInvetarioOtro)
      .subscribe(response => {
        console.log(response.result);
        loader.dismiss();
        this.navCtrl.setRoot(HomePage);
      });
  }

   EnviarInventarioQuasar(){
    let loader = this.loadingCtrl.create();
    loader.present();
     
    this.PTInventory.postTInventory(this.dataInventory)
      .subscribe(response => {
        loader.dismiss();
        this.navCtrl.setRoot(HomePage);
    });
  }

}
