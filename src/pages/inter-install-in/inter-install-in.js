var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Platform, AlertController, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionService } from "../../services/session.service";
import { TasksInstallService } from "../../services/tasksinstall.service";
import { TasksUnistallService } from "../../services/tasksunistall.service";
import { TasksMeasuService } from "../../services/tasksmeasurements.service";
import { TasksInventoryService } from "../../services/tasksinventary.service";
import { TasksIssue } from "../../services/tasksissue.service";
import { CMotiveService } from '../../services/campaignsmotive.service';
import { CSubMotiveService } from '../../services/campaignssubmotive.service';
import { CTargetService } from '../../services/campaignsissuetarget.service';
import { CStatus } from '../../services/campaignsissuestatus.service';
import { CCAtegoriaService } from '../../services/campaignsissuecategoria.service';
import { CInstallStatusService } from '../../services/campaignsinstallstatus.service';
import { HomePage } from '../home/home';
import { FileUploader } from 'ng2-file-upload';
import { AppSettings } from '../../app.settings';
import { CampaignsCode } from '../../services/bycode.service';
/**
 * Generated class for the InterInstallPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var InterInstallIn = (function () {
    function InterInstallIn(sessionS, tasksI, tasksU, tasksM, tasksIn, navCtrl, navParams, plt, camera, taskIs, CMotive, CSubMotive, CTarget, CStatus, CScategoria, CInstallStatus, alertCtrl, loadingCtrl, campaignscode) {
        this.sessionS = sessionS;
        this.tasksI = tasksI;
        this.tasksU = tasksU;
        this.tasksM = tasksM;
        this.tasksIn = tasksIn;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.plt = plt;
        this.camera = camera;
        this.taskIs = taskIs;
        this.CMotive = CMotive;
        this.CSubMotive = CSubMotive;
        this.CTarget = CTarget;
        this.CStatus = CStatus;
        this.CScategoria = CScategoria;
        this.CInstallStatus = CInstallStatus;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.campaignscode = campaignscode;
        this.uploader = new FileUploader({ url: AppSettings.BASE_PATH + AppSettings });
        this.dataSend = {
            campaign: '',
            code: '',
            store: '',
            file: '',
            target: '',
            comment: '',
            motive: '',
            submotive: '',
            install_status: ''
        };
        this.dataSendInstall = {
            campaign: '',
            code: '',
            store: '',
            comment: '',
            file: '',
            target: ''
        };
        this.dataInvetarioOtro = {
            target: 'Inventario',
            store: this.sessionS.getObject('tienda').store._id,
            file: '',
            motive: 'Elemento no instalado por Quasar',
            submotive: 'Instalado por proveedor no autorizado',
            element: '',
            quantity: null,
            material: '',
            client: '',
            brand: '',
            responsable: '',
            signed: false,
            comment: '',
            category: ''
        };
        this.image = '';
        this.type_task = '';
        this.campaingsName = '';
        this.location = [];
        this.motiveA = [];
        this.campaings = [];
        this.campaings2 = [];
        this.SubmotiveA = [];
        this.targetA = [];
        this.statusA = [];
        this.categoriaA = [];
        this.statusInstall = [];
        this.Materiales = [];
        this.Cliente = [];
        this.Element = [];
        this.CategoriI = [];
        this.namestore = [];
        this.userFilter = { name: '' };
    }
    InterInstallIn.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.type_task = this.navParams.get('task');
        console.log('ionViewDidLoad InterInstall hh');
        this.namestore = this.sessionS.getObject('tienda').store.name;
        this.dataSend.campaign = "";
        this.dataSendInstall.campaign = "";
        this.dataSend.code = "";
        this.dataSendInstall.code = "";
        this.dataSend.store = this.sessionS.getObject('tienda').store._id;
        this.dataSendInstall.store = this.sessionS.getObject('tienda').store._id;
        this.dataSend.target = this.type_task;
        this.dataSendInstall.target = this.type_task;
        this.dataSend.motive = '';
        this.dataSend.comment = '';
        this.dataSendInstall.comment = '';
        this.dataSend.submotive = '';
        this.dataSend.install_status = '';
        this.CInstallStatus.getCIStatus()
            .subscribe(function (response) {
            if (_this.type_task == 'Instalación') {
                _this.statusInstall = response.result;
            }
            else if (_this.type_task == 'Desinstalación' || _this.type_task == 'Inventario') {
                _this.statusInstall = ['REALIZADO', 'NO REALIZADO'];
            }
            console.log(_this.statusInstall);
            _this.CStatus.getCampaings()
                .subscribe(function (response) {
                _this.campaings = response.result;
                _this.campaings2 = response.result;
                console.log("esteeste");
                console.log(_this.campaings);
                _this.CStatus.getIStatus()
                    .subscribe(function (response) {
                    _this.statusA = response.result;
                    _this.CMotive.getCMotive()
                        .subscribe(function (response) {
                        _this.motiveA = response.result;
                        console.log(_this.motiveA);
                        _this.CSubMotive.getCSubMotive()
                            .subscribe(function (response) {
                            _this.SubmotiveA = response.result;
                            console.log(_this.SubmotiveA);
                        });
                    });
                });
            });
        });
        this.tasksIn.getMaterialsInventary()
            .subscribe(function (response) {
            _this.Materiales = response.result;
            console.log(_this.Materiales);
        });
        this.tasksIn.getClienteInventary()
            .subscribe(function (response) {
            _this.Cliente = response.result;
            console.log(_this.Cliente);
        });
        this.tasksIn.getElementInventary()
            .subscribe(function (response) {
            _this.Element = response.result;
            console.log(_this.Element);
        });
        this.tasksIn.getCategoriInventary()
            .subscribe(function (response) {
            for (var i = 0; i < response.result.length; i++) {
                for (var k = 0; k < response.result[i].categories.length; k++) {
                    _this.CategoriI.push(response.result[i].categories[k].name);
                }
            }
        });
    };
    InterInstallIn.prototype.funcionInstalado = function () {
        var _this = this;
        this.type_task = this.sessionS.getItem('type_task');
        this.tasksI.setInstall(this.dataSendInstall)
            .subscribe(function (response) {
            console.log(response.result);
            _this.dataSendInstall.comment = _this.dataSend.comment;
        });
        var a = this.navCtrl.setRoot(HomePage);
        var alert = this.alertCtrl.create({
            title: 'Mensaje',
            subTitle: 'Instalacion Exitosa',
            buttons: ['ok']
        });
        alert.present();
    };
    InterInstallIn.prototype.foto = function () {
        var _this = this;
        this.plt.ready().then(function (readySource) {
            _this.camera.getPicture({
                destinationType: _this.camera.DestinationType.DATA_URL,
                targetWidth: 1000,
                targetHeight: 1000,
                correctOrientation: true
            }).then(function (imageData) {
                _this.dataSend.file = imageData;
                _this.dataSendInstall.file = imageData;
                _this.dataInvetarioOtro.file = imageData;
                _this.image = "data:image/jpeg;base64," + imageData;
                console.log(_this.image);
            }, function (err) {
                console.log(err);
            });
        });
    };
    InterInstallIn.prototype.enviarInstalacion = function () {
        this.taskIs.postTasksIssue(this.dataSend)
            .subscribe(function (response) {
            console.log(response.result);
        });
        var a = this.navCtrl.setRoot(HomePage);
        var alert = this.alertCtrl.create({
            title: 'Mensaje',
            subTitle: 'Su Incidencia a sido enviada',
            buttons: ['ok']
        });
        alert.present();
    };
    InterInstallIn.prototype.buscarCode = function (id, code) {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.campaignscode.getCampaignsCode(id)
            .subscribe(function (response) {
            _this.campaingsName = response.result.name;
            _this.dataSendInstall.campaign = response.result._id;
            _this.dataSend.campaign = _this.campaingsName;
            _this.dataSendInstall.code = _this.dataSend.code;
            console.log(response.result.elements);
            _this.location = response.result.elements.filter(function (item) { return item.code == _this.dataSend.code.split('-')[0]; })[0].location;
            loader.dismiss();
            console.log(_this.location);
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Error en la Busqueda',
                subTitle: 'Campaña no Existe',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    InterInstallIn.prototype.funcionInventarioOtro = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({});
        loader.present();
        this.taskIs.postTasksIssue(this.dataInvetarioOtro)
            .subscribe(function (response) {
            console.log(response.result);
            loader.dismiss();
            _this.navCtrl.setRoot(HomePage);
        });
    };
    return InterInstallIn;
}());
InterInstallIn = __decorate([
    IonicPage(),
    Component({
        selector: 'page-inter-install-in',
        templateUrl: 'inter-install-in.html',
        providers: [Camera]
    }),
    __metadata("design:paramtypes", [SessionService,
        TasksInstallService,
        TasksUnistallService,
        TasksMeasuService,
        TasksInventoryService,
        NavController,
        NavParams,
        Platform,
        Camera,
        TasksIssue,
        CMotiveService,
        CSubMotiveService,
        CTargetService,
        CStatus,
        CCAtegoriaService,
        CInstallStatusService,
        AlertController,
        LoadingController,
        CampaignsCode])
], InterInstallIn);
export { InterInstallIn };
//# sourceMappingURL=inter-install-in.js.map