var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Broadcaster } from "./broacaster";
import { Injectable } from "@angular/core";
var UserProfile = UserProfile_1 = (function () {
    function UserProfile(broadcaster) {
        this.broadcaster = broadcaster;
    }
    UserProfile.prototype.fire = function (value) {
        this.broadcaster.broadcast(UserProfile_1, value);
    };
    UserProfile.prototype.on = function () {
        return this.broadcaster.on(UserProfile_1);
    };
    return UserProfile;
}());
UserProfile = UserProfile_1 = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Broadcaster])
], UserProfile);
export { UserProfile };
var UserProfile_1;
//# sourceMappingURL=user-profile.js.map