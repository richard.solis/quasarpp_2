import {Observable} from 'rxjs/Observable';
import {Broadcaster} from "./broacaster";
import {Injectable} from "@angular/core";

@Injectable()
export class UserProfile {
  constructor(private broadcaster: Broadcaster) {}

  fire(value: any): void {
    this.broadcaster.broadcast(UserProfile, value);
  }

  on(): Observable<any> {
    return this.broadcaster.on<any>(UserProfile);
  }
}
