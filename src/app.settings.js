var AppSettings = (function () {
    function AppSettings() {
    }
    return AppSettings;
}());
export { AppSettings };
AppSettings.BASE_PATH = 'http://dev.atypax.com:3036/';
//public static BASE_PATH = 'http://192.168.1.79:3036/';
//public static BASE_PATH = 'http://192.168.1.15:3036/';
AppSettings.LOGIN = 'users/login';
AppSettings.ROUTE = 'routes/my/';
AppSettings.TASKSSTORE = 'tasks/';
AppSettings.TASKSINSTALL = 'tasks/install/';
AppSettings.TASKSUNISTALL = 'tasks/uninstall/';
AppSettings.TASKSINVENTORY = 'tasks/inventory/';
AppSettings.TASKSCONFIRM = 'tasks/confirm/';
AppSettings.TASKSMEASUREMENTS = 'tasks/measurements/';
AppSettings.TASKSISSUE = 'tasks/issue/';
AppSettings.CAMPAIGNS = 'campaigns/';
AppSettings.CAMPAIGNSMOTIVE = 'campaigns/motive/';
AppSettings.CAMPAIGNSSUBMOTIVE = 'campaigns/submotive/';
AppSettings.CAMPAIGNSTARGET = 'campaigns/issue/target/';
AppSettings.CAMPAIGNSSTATUS = 'campaigns/issue/status/';
AppSettings.CAMPAIGNSCATEGORIA = 'campaigns/categories/';
AppSettings.CAMPAIGNSINSTALLSTATUS = 'campaigns/install/status/';
AppSettings.CAMPAIGNSIMPLEMENTERS = 'campaigns/implementers/';
AppSettings.TASKSCONTACTS = 'tasks/contacts/';
AppSettings.STORECONTACTS = 'stores/contact/';
AppSettings.PTASKSCONFIRM = 'tasks/confirm/';
AppSettings.PTASKSINVENTORY = 'tasks/inventory/';
AppSettings.PTASKSMEASUREMENT = 'tasks/measurement/';
AppSettings.TASKSUNISTALLPOST = 'tasks/uninstall/';
AppSettings.OPPORTUNITIES = 'opportunities/';
AppSettings.OPPORTUNITIESGET = 'opportunities/type/';
AppSettings.MAINTANANCE = 'tasks/maintenance/';
AppSettings.BYCODE = 'campaigns/bycode/';
AppSettings.MATERIALSINVENTORY = 'inventories/materials/';
AppSettings.CLIENTSINVENTORY = 'clients/';
AppSettings.ELEMENTSINVENTORY = 'elements/';
AppSettings.CATEGORYINVENTORY = 'campaigns/categories/';
AppSettings.DETAILMEASUREMENTS = 'measurements/detail/';
AppSettings.CONTACTPRUEBA = 'stores/contact/positions/';
AppSettings.LOCALISATION = 'stores/near/';
AppSettings.TOUCH = 'navigations/';
//# sourceMappingURL=app.settings.js.map